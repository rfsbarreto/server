from distutils.core import setup

setup(
	name='RotaFacil',
	version='1.0',
	description='A Python library of RotaFacil',
	author='Adolfo Guimaraes',
	author_email='adolfoguimaraes@gmail.com',
	url='http://rotafacil-adolfodev.rhcloud.com',
	py_modules=['RotaFacil']
	)