/* 

	JavaScript - RotaCerta v2.0
	@author Adolfo Guimarães
	@email adolfoguimaraes@gmail.com
	@date-create 29.07.11
	@date-modify 17.06.13

*/

var map;
var geo;


var pointsOfInterest = [];
var pointsOfGoogleRoute = [];
var pointsOfRouteLine = [];
var references = [];
var pointsOfRC = [];
var newPointsOfRC = [];
var circleRadius = 0.2;
var start_address;
var end_address;
var pontosReferencia = [];

var json_out = {}

/*
	loadMap(centerLat, centerLong, zoom)
	Cria o mapa na view showmap e coloca o ponto inicial nas coordenadas centerLat e centerLong e com um zoom inicial de zoom.
	Os valores são determinados na admin do sistema.


*/
	
	function loadMap(lat, lng, z) {
		var latlng = new google.maps.LatLng(lat, lng);
		
		
		var defaultBounds = new google.maps.LatLngBounds(new google.maps.LatLng(lat, lng));
		
		var input_from = document.getElementById('from');
		var input_to = document.getElementById('to');
		
		var options = {
  			bounds: defaultBounds,
  			types: ['establishment','geocode']
		};

		autocomplete = new google.maps.places.Autocomplete(input_from, options);
		autocomplete = new google.maps.places.Autocomplete(input_to, options);
		
		
		
		
		
		
		var myOptions = {
      			zoom: z,
      			center: latlng,
      			mapTypeId: google.maps.MapTypeId.ROADMAP
    		};

    		
		map = new google.maps.Map(document.getElementById("map_canvas"),
        					myOptions);
	

		//loadPointsOfInterest(map)
	}

	/*

		Retorna a distância entre dois pontos
	*/

	function getDistance(pointA, pointB) {
		distance = google.maps.geometry.spherical.computeDistanceBetween(pointA, pointB);
		return distance;
		
	}


	/*

		Cria uma MARKER no map
		
		Tipos
			A: Ponto inicial da Rota
			B: Ponto final da Rota
			R: Ponto de referência no mapa			

	*/
	function createMarker(latLng, title, type) {
		var icon;
			
		if(type == "A") 
			icon = "http://www.google.com/mapfiles/markerA.png";
		else if(type == "B")
			icon = "http://www.google.com/mapfiles/markerB.png";
		else if(type == "R")
			icon = "/static/img/rota_facil/house1.png";

		var contentString =
			'<h5 id="firstHeading" class="firstHeading"><strong>' + title + '</strong></h5>';

		var infoWindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker = new google.maps.Marker({
			position: latLng,
			title:title,
			icon:icon
		});

		google.maps.event.addListener(marker, 'click', function() {
			infoWindow.open(map,marker);
		});

		

		return marker
	}

	/*
	
		Carrega os pontos de referência do arquivo ref.kml

		A função chama a view getPointsOfInterest do Python


	*/
	
	function loadPointsOfInterest(map) { 
		
		new Ajax.Request('/get_points/',{
			method: 'get',
			evalJSON: 'force',
			onSuccess: function(transport) {
				var json = transport.responseText.evalJSON(true);
				var all = json.toArray();
				var id = 0;
				for(var i = 0; i < all.length; i++) {
					
					var latlng = new google.maps.LatLng(all[i]['latitude'],all[i]['longitude']);
	
					var point = { 
						'name': all[i]['name'],
						'point': latlng,
					}

					pointsOfInterest.push(point);

					id = id + 1;
				}
			},
			onFailure: function() {
				alert('Ajax Failure!'); 
			}
			
		});

	}

	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),results = regex.exec(location.search);
    	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}


	/*

		Retorna a rota do caminho solicitado

		Chama a view getRoute do Python

	*/

	function getDirections() {

		document.getElementById("msg").style.display = '';
		document.getElementById("msg").innerHTML = "Aguarde ... ";
		

		var from = retiraAcento(document.getElementById("from").value);
		var to = retiraAcento(document.getElementById("to").value);

			
		var parameter = "from=" + from + "&to=" + to;
        
        
		new Ajax.Request('/rota_facil/get_route/', {
			method: 'get',
			evalJSON: 'force',          
			parameters: parameter,
			onSuccess: function(transport) {

					
				var json = transport.responseText.evalJSON(true);	
				
				var points = json['polyline'].toArray();
				var google_route = json['google_route'].toArray();
				start_address = json['start_address'];
				end_address = json['end_address'];
				


				var numberOfPoints = points.length;
				var newCenter;
				var indice;
				if(numberOfPoints % 0 == 2)
					indice = numberOfPoints / 2;					
				else
					indice = (numberOfPoints+1)/2;
				

				//Indica um novo centro para o mapa. O novo centro é mais ou menos no meio da nova rota
				newCenter = new google.maps.LatLng(points[indice][0],points[indice][1]);

				

				for(var i = 0; i < points.length; i++) { 
					var latlng = new google.maps.LatLng(points[i][0],points[i][1]);
					


					if(i == 0) {
						var marker = createMarker(latlng, start_address, "A");
						marker.setMap(map);
					}
					else if(i == points.length - 1) {
						var marker = createMarker(latlng, end_address, "B");
						marker.setMap(map);
					}
					
					pointsOfRouteLine.push(latlng);


				}
				
				//alert(pointsOfRouteLine);
				for(var j = 0; j <  google_route.length; j++) { 
					var p = new google.maps.LatLng(google_route[j]['point']['lat'],google_route[j]['point']['lng']);
					
					var pointGoogle = { 
						'text': google_route[j]['text'],
						'point': p,
					}

					pointsOfGoogleRoute.push(pointGoogle);
				}				

				
				//Chamada da função MarcaPonto
				marcaPonto(0,1);
				
				//Chamada da função que vai gerar o novo texto
				generateText();
				
				//Chamada da funcao que vai setar os markers no mapa
				printReferencesMarkers();
				
				//Desenha a linha da rota no mapa
				var route_path = new google.maps.Polyline({
					path: pointsOfRouteLine,
					strokeColor: "#FF0000",
					strokeOpacity: 1.0,
					strokeWeigth: 2
				});
				route_path.setMap(map);

				
				map.setCenter(newCenter);
				map.setZoom(map.getZoom() + 3);
				
				document.getElementById("msg").style.display = 'none';

			},
			onFailure: function() {
				document.getElementById("msg").style.display = '';
				document.getElementById('msg').innerHTML = 'Erro ao recuperar a rota.';
			}
		});
	}

	
	function generateText() {
		


		var type = -1;
		var context = "";
		var distance = -1;
	
		for(i = 0; i < pointsOfRC.length; i++) {

			atualPoint = pointsOfRC[i];
			//alert(atualPoint['point'].toString());


			if(atualPoint['type'] != 'key') {
				type = 0;
			}
			else {
				type = 1;
			}

			if(i == 0) {
				context = "begin";
			}
			else if( i == pointsOfRC.length - 1) {
				context = "final";
			}
			else {
				if(type == 1) {
					var palavraChave = (atualPoint['google_text'].toString()).split(" ")[0];
					if( palavraChave == "Vire" || palavraChave == "Pegue" || palavraChave == "Curva") {						
						context = "corner";
					}
					else {
						context = "normal";
					}
				}
				else {
					context = "normal";

				}

			}


			if(context != "final") {
				distance = getDistance(atualPoint['point'], pointsOfRC[i+1]['point']);
			}
			
			var distanceNext = -1;
			var distancePrev = -1;

			var newReferences = [];

			references = atualPoint['references'];

			for(var r = 0; r < references.length; r++) {				
				if(context == "begin") { 
					distanceNext = getDistance(references[r]['point'],pointsOfRC[i+1]['point']);
				}
				else if(context == "final") {
					distancePrev = getDistance(references[r]['point'],pointsOfRC[i-1]['point']);
				}
				else {
					distanceNext = getDistance(references[r]['point'],pointsOfRC[i+1]['point']);
					distancePrev = getDistance(references[r]['point'],pointsOfRC[i-1]['point']);
				}

				
				//var refmarker = createMarker(references[r]['point'],references[r]['name'], "R");				
				//refmarker.setMap(map);
				reference = {
					'id': references[r]['id'],
					'name': references[r]['name'],
					'point': references[r]['point'],
					'distance': references[r]['distance'],
					'distancePrev': distancePrev,
					'distanceNext': distanceNext,
				}

				newReferences.push(reference);
			}

			newPointOfRC = {
				'point': pointsOfRC[i]['point'],
				'reference': newReferences,
				'type': type,
				'context': context,
				'google_text': pointsOfRC[i]['google_text'],
				'distanceNext': distance,
			}




			newPointsOfRC.push(newPointOfRC);

		}


		var pontoInicial = newPointsOfRC[0]['point']
		p = retiraCaracterInvalido(Object.toJSON(newPointsOfRC));		
		
		new Ajax.Request('/rota_facil/get_text/', {
			method: 'post',
			evalJSON: 'force',
			parameters: "p="+p+"&start="+start_address+"&end="+end_address+"&ponto="+pontoInicial,
			asynchronous : false,
			onSuccess: function(transport) {
				var text = transport.responseText;
				document.getElementById('google_text').innerHTML = text;	
				document.getElementById('msg').innerHTML = "";
			},
			onFailure: function() {
				document.getElementById("msg").style.display = '';
				document.getElementById('msg').innerHTML = 'Erro ao recuperar ao texto'
			}
		});
	
	}

	function getReferences(point) {

	references = [];
	var parameter = "lat=" + point.lat() + "&lng=" + point.lng();

	new Ajax.Request('/rota_facil/get_places/', {
		method : 'get',
		evalJSON : 'force',
		parameters : parameter,
		asynchronous : false,
		onSuccess : function(transport) {

			var json = transport.responseText.evalJSON(true);
			var places_points = json.toArray();
			var id = 0;

			for ( var i = 0; i < places_points.length; i++) {
				var latlng = new google.maps.LatLng(
						places_points[i]['latitude'],
						places_points[i]['longitude']);
				var distance = getDistance(point, latlng);

				if (distance <= circleRadius * 500) {
					referenceSelect = {
						'id' : id,
						'name' : places_points[i]['name'],
						'point' : latlng,
						'distance' : distance,
					}

					references.push(referenceSelect);
					id = id + 1;
				}
			}
		},
		onFailure : function() {
			alert('Ajax Failure! Erro na requisição do Google Places');
		}
	});
	return references;

}


	function marcaPonto(v1, v2) {
		var totalLines = pointsOfRouteLine.length;
	
	
		var c1;
		var c2;

		if(v1 == totalLines - 1) { 
			c1 = pointsOfRouteLine[v1];

			keyIndex = ehKeyPoint(c1);
			
			if(keyIndex != -1) {
                
				references = getReferences(c1);
				
				pointRC = {
					'point': c1,
					'references': references,
					'type': 'key',
					'google_text': pointsOfGoogleRoute[keyIndex]['text'],
				}
			}

			else {
                
                references = getReferences(c1);
				
				pointRC = {
					'point': c1,
					'references': references,
					'type': 'not-key',
					'google_text': 'none',
				}


			}

			pointsOfRC.push(pointRC);

		}
		else {

			c1 = pointsOfRouteLine[v1];
			c2 = pointsOfRouteLine[v2];

		
			var dPontos = getDistance(c1, c2);

		
			var diametro = (circleRadius)*1000;


			var keyIndex = ehKeyPoint(c1);
			var keyIndex2 = ehKeyPoint(c2);

		

			if(keyIndex != -1) {

				references = getReferences(c1);
				
				pointRC = {
					'point': c1,
					'references': references,
					'type': 'key',
					'google_text': pointsOfGoogleRoute[keyIndex]['text'],
				}


				pointsOfRC.push(pointRC);


				var f = v2;
				var s = v2+1;

				marcaPonto(f, s);

			}
			else {
				if(v2 == totalLines - 1) {


					keyIndex = ehKeyPoint(c2);
                    
					references = getReferences(c2);

					if(keyIndex != -1) {

						pointRC = {
							'point': c2,
							'references': references,
							'type': 'key',
							'google_text': pointsOfGoogleRoute[keyIndex]['text'],
						}

						pointsOfRC.push(pointRC);

					}
					else {
				
						pointRC = { 
							'point': c2,
							'references': references,
							'type': 'not-key',
							'google_text': 'none',

						}

						pointsOfRC.push(pointRC);
					

					}
				}
				else if (dPontos >= diametro) {
					
					references = getReferences(c1);

					pointRC = {
						'point':c1,
						'references': references,
						'type': 'not-key',
						'google_text': 'none',
					}

					pointsOfRC.push(pointRC);
					
					keyIndex = ehKeyPoint(c2);

					if(keyIndex != -1) {
						marcaPonto(v1+1,v1+2);
					}	
					else { 
						marcaPonto(v2, v2+1);
					}
				}
				else {
					keyIndex = ehKeyPoint(c2);
					if(keyIndex != -1) {

						references = getReferences(c1);

						pointRC = {
							'point': c1,
							'references': references,
							'type': 'key',
							'google_text': pointsOfGoogleRoute[keyIndex]['text'],
						}	

						pointsOfRC.push(pointRC);
	
						marcaPonto(v2, v2+1);
					}
					else {
						marcaPonto(v1,v2+1);
					}



				}
			}
		}

	}

	function ehKeyPoint(point) {
		var retorno = -1;
		for(var j = 0; j < pointsOfGoogleRoute.length; j++) {	
			var p2 = pointsOfGoogleRoute[j]['point']; //new google.maps.LatLng(points_of_google[j]['point']['lat'], points_of_google[j]['point']['lng']);
			var dist = getDistance(point, p2);
			
			if (dist < 0.1) {
				retorno = j
				break;
			
			}
		}

		return retorno;

	}

	

function retiraAcento(Campo){
            var Acentos ="áàãââÁÀÃÂéêÉÊíÍóõôÓÔÕúüÚÜçÇabcdefghijklmnopqrstuvxwyz";
    		var Traducao ="AAAAAAAAAEEEEIIOOOOOOUUUUCCABCDEFGHIJKLMNOPQRSTUVXWYZ";
    		var Posic,Carac;
    		var TempLog = "";
    		
    for (var i=0; i < Campo.length; i++) {
        Carac = Campo.charAt(i);
        			Posic=Acentos.indexOf(Carac);
        			if(Posic>-1)
            			TempLog+=Traducao.charAt(Posic);
        		else
            			TempLog += Campo.charAt(i);
        	}
		
    return (TempLog);
}

function retiraCaracterInvalido(Campo){
    var Acentos ="&";
	var Traducao ="E";
	var Posic,Carac;
	var TempLog = "";
	
for (var i=0; i < Campo.length; i++) {
Carac = Campo.charAt(i);
			Posic=Acentos.indexOf(Carac);
			if(Posic>-1)
    			TempLog+=Traducao.charAt(Posic);
		else
    			TempLog += Campo.charAt(i);
	}

return (TempLog);
}

function printReferencesMarkers() {

	new Ajax.Request('/rota_facil/get_markers/', {
		method : 'get',
		evalJSON : 'force',
		asynchronous : false,
		onSuccess : function(transport) {

			var json = transport.responseText.evalJSON(true);
			var markers = json.toArray();

			for ( var i = 0; i < markers.length; i++) {
				var latlng = new google.maps.LatLng(
						markers[i]['lat'],
						markers[i]['lng']);

				var refmarker = createMarker(latlng,markers[i]['name'], "R");				
				refmarker.setMap(map);
				}			
		},
		onFailure : function() {
			alert('Ajax Failure! Erro na requisição do References Markers');
		}
	});
}


