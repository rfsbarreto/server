from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from register.models import Projects, UserProject
from django.http import Http404
from django.core.paginator import Paginator

import register.lib.groups as r_groups
import register.lib.projects as r_projects

import os

from django.template.context import RequestContext

def home(request):

    return render(request, 'index.html', {})

def loginpage(request):
    return render(request, 'login.html', {})

def about(request):
    return render(request, 'about.html', {})

def authuser(request):
    username = request.POST['username']
    password = request.POST['userpassword']

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect('/main/')
        else:
            return render(request, "index.html", {'status':'noactive'})
    else:
        return render(request, "index.html", {'status':'error'})

def logoutuser(request):
    logout(request)
    return render(request,"index.html")


def main(request):
    if not request.user.is_authenticated():
        return render(request, "index.html")
    else:
        
        list_projects = []
        list_public_projects = []

        projects = r_projects.get_all_projects()



        is_developer = r_groups.is_developer(request.user)

        for p in projects:
            if p.active == 1:
                dict = {
                    'title': p.title,
                    'identifier': p.project_identifier,
                }
                list_projects.append(dict)
                if p.has_public == 1:
                    list_public_projects.append(dict)
            
        
        return render(request, "main.html", {'menu':'','projects':list_projects,'public_projects':list_public_projects, 'is_developer':is_developer})

def project(request, projectid):
    return redirect("/go/"+projectid+"/",{'projectid':projectid})

def public(request, projectid):
    return redirect("/go/"+projectid+"/public/",{'projectid':projectid})
    