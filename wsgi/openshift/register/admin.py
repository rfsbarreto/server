from django.contrib import admin
from register.models import Projects, UserProject

# Register your models here.
admin.site.register(Projects)
admin.site.register(UserProject)