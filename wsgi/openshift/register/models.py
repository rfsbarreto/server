from django.db import models
from django.contrib.auth.models import User
from django_extensions.db.fields import UUIDField

# Create your models here.
class Projects(models.Model):
    title = models.CharField(max_length=50)
    project_identifier =  models.CharField(max_length=50)
    go_key = UUIDField(version=4)
    active = models.BooleanField()
    created_by = models.ForeignKey(User)
    date_created = models.DateField()
    has_public = models.BooleanField()
    
    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural="Projects"
        
class UserProject(models.Model):
    user = models.ForeignKey(User)
    project = models.ForeignKey(Projects)

    def __unicode__(self):
        return self.user.first_name +  " (" + self.user.username + ") --> " + self.project.title + " (" + self.project.project_identifier + ")"