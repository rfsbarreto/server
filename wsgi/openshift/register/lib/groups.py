

def is_developer(user):
    return user.groups.filter(name='Developer')

def is_admin(user):
    return user.groups.filter(name='Admin')

def is_public(user):
    return user.groups.filter(name='Public')