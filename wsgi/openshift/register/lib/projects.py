from register.models import Projects, UserProject

def get_all_projects():
    return Projects.objects.all()

def get_projects_of_user(user):
    return UserProject.objects.filter(user=user.id)


def get_users_of_project(project):
    project_dict = Projects.objects.filter(project_identifier=project).values()[0]
    project_id = project_dict['id']

    user_projects = UserProject.objects.filter(project=project_id)

    list_user_projects = []

    for p in user_projects:
        user = {
            'first_name': p.user.first_name,
            'last_name': p.user.last_name,
            'username': p.user.username,
        }

        list_user_projects.append(user)

    return list_user_projects
