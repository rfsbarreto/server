from django.conf.urls import patterns, include, url
from projects import observatory
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'web.views.home', name='home'),
    url(r'^about/', 'web.views.about', name='about'),
    url(r'^login/', 'web.views.loginpage', name='login'),
    url(r'^authuser/', 'web.views.authuser', name='authuser'),
    url(r'^logout/', 'web.views.logoutuser', name='logout'),
    url(r'^main/', 'web.views.main', name='main'),
    url(r'^project/(?P<projectid>[a-z|_]+)/$','web.views.project', name='project'),
    url(r'^project/(?P<projectid>[a-z|_]+)/public/$','web.views.public', name='public'),
    url(r'^go/rota_facil/', include('projects.rota_facil.urls')),
    url(r'^go/go_track/', include('projects.go_track.urls')),
    url(r'^go/go_bus/', include('projects.go_bus.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^go/observatory/', include('projects.observatory.urls'))
    # url(r'', include('mama_cas.urls')),
)
