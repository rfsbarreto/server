#coding=utf-8
from django.shortcuts import render
from django.conf import settings
from collections import Counter
import operator

from projects.rota_facil.lib.RotaFacil import RotaFacil

from projects.go_track.models import Tracks, TracksPoints

import os.path

if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    PROJECT_PATH = os.environ['OPENSHIFT_REPO_DIR']+"wsgi/openshift"
else:
    PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))


URL_PAGE = PROJECT_PATH+"/projects/observatory/templates/observatory.html"
URL_WEB = PROJECT_PATH+"/templates/project.html"
PROJECT_NAME = "Observatório"
PROJECT_ID = "observatory"


def home(request):

    args = {'projectid':PROJECT_ID, 'projectname':PROJECT_NAME, 'urlpage': URL_PAGE}
    args['packages_dir'] = settings.PACKAGES_DIR

    return render(request, URL_WEB, args)

def public(request, cityid):

    PUBLIC_PAGE = PROJECT_PATH+"/projects/observatory/templates/public/home.html"
    args = {'projectid':PROJECT_ID, 'projectname':PROJECT_NAME, 'urlpage': PUBLIC_PAGE}

    if(cityid == "public"):
        args['city'] = "public"
    else:
        if (cityid == "aracaju"):
            args['city'] = "Aracaju"

            config = {
                'map_center': {'lat': -10.947247, 'lng': -37.073082},
                'map_width': 820,
                'map_height': 560,
                'heatmap': True,
                'map_zoom': 13,
                'map_type': "satellite",
            }

            q_car = TracksPoints.objects.filter(track__car_or_bus=1)
            q_bus = TracksPoints.objects.filter(track__car_or_bus=2)
            q_bus2 = TracksPoints.objects.filter(track__car_or_bus=0)

            list_points = []

            for p in q_car:
                list_points.append((p.latitude, p.longitude))

            for p in q_bus:
                list_points.append((p.latitude, p.longitude))

            for p in q_bus2:
                list_points.append((p.latitude, p.longitude))

        rf = RotaFacil(config)
        rf.setHeatMapPoints(list_points)

        dict = Counter(list_points)


        sorted_dict = sorted(dict.items(), key=operator.itemgetter(1))

        sorted_dict.reverse()

        final_list = list(sorted_dict)[:50]

        dict_list = []

        address_map = {}

        for p in final_list:

            dict = {
                'point': p[0],
                'address': rf.getAddress((p[0][0], p[0][1])),
                'frequency': p[1]
            }

            dict_list.append(dict)

            address = dict['address']

            if address_map.has_key(address):
                address_map[address]['frequency'] += dict['frequency']
                address_map[address]['points'].append((dict['point'], dict['frequency']))
            else:
                address_map[address] = {'frequency': dict['frequency'], 'points': [(dict['point'], dict['frequency'])]}


        sorted_adress_map = sorted(address_map.items(), key=lambda k: k[1]['frequency'])

        sorted_adress_map.reverse()

        args['total_points'] = len(list_points)
        args['most_visited'] = sorted_adress_map

        google_js = rf.googleJS()
        rotafacil_js = rf.rotaFacilJS()
        draw_map = rf.drawMap()

        args['google_js'] = google_js
        args['rotafacil_js'] = rotafacil_js
        args['draw_map'] = draw_map

    return render(request, URL_WEB, args)