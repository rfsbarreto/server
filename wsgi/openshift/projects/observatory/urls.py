from django.conf.urls import patterns, include, url


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('projects.observatory.views',
    url(r'^$', 'home', name='observatory_home'),
    url(r'(?P<cityid>public)/$','public',name='observatory_public'),
    url(r'(?P<cityid>aracaju)/$','public',name='observatory_aracaju'),

)