<?xml version="1.0" encoding="ISO-8859-1"?>

   <xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	exclude-result-prefixes="xs xdt err fn">
		
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="/">
	<ReferencesMarkers>	
	<xsl:for-each select="ReferencesBase/Marker">

		<xsl:choose>
			
			<xsl:when test="@context = 'begin'">
				<xsl:choose>
					<xsl:when test="count(Reference) > 0">
						<Reference>
						<name><xsl:value-of select="Reference/name/text()" /></name>
						<lat><xsl:value-of select="Reference/@lat"></xsl:value-of></lat>
						<lng><xsl:value-of select="Reference/@long"></xsl:value-of></lng> 
						<address><xsl:value-of select="Reference/address/text()" /></address>
						</Reference>												
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@context = 'final'">
				<xsl:choose>
					<xsl:when test="count(Reference) > 0">
						<Reference>
						<name><xsl:value-of select="Reference/name/text()" /></name>
						<lat><xsl:value-of select="Reference/@lat"></xsl:value-of></lat>
						<lng><xsl:value-of select="Reference/@long"></xsl:value-of></lng> 
						<address><xsl:value-of select="Reference/address/text()" /></address>
						</Reference>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@context = 'corner'">
				<xsl:choose>
					<xsl:when test="count(Reference) > 0">
						<Reference>
						<name><xsl:value-of select="Reference/name/text()" /></name>
						<lat><xsl:value-of select="Reference/@lat"></xsl:value-of></lat>
						<lng><xsl:value-of select="Reference/@long"></xsl:value-of></lng> 
						<address><xsl:value-of select="Reference/address/text()" /></address>
						</Reference>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@context = 'normal'" >
				<xsl:variable name="contextonormal" select="normalContext/text()" />
				<xsl:choose>
					<xsl:when test="$contextonormal = 'normal'">
						<xsl:choose>
							<xsl:when test="count(Reference) > 0">
								<Reference>
								<name><xsl:value-of select="Reference/name/text()" /></name>
								<lat><xsl:value-of select="Reference/@lat"></xsl:value-of></lat>
								<lng><xsl:value-of select="Reference/@long"></xsl:value-of></lng> 
								<address><xsl:value-of select="Reference/address/text()" /></address>
								</Reference>
							</xsl:when>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$contextonormal = 'longpath'">
						<xsl:choose>
							<xsl:when test="count(Reference) > 0">
								<Reference>
								<name><xsl:value-of select="Reference/name/text()" /></name>
								<lat><xsl:value-of select="Reference/@lat"></xsl:value-of></lat>
								<lng><xsl:value-of select="Reference/@long"></xsl:value-of></lng> 
								<address><xsl:value-of select="Reference/address/text()" /></address>
								</Reference>
							</xsl:when>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
		</xsl:for-each>
		</ReferencesMarkers>
	</xsl:template>
	
	
	
</xsl:stylesheet>
	
	
