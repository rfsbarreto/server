<?xml version="1.0" encoding="ISO-8859-1"?>

   <xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	exclude-result-prefixes="xs xdt err fn">
	
	
	<xsl:template match="/">

	<ul class="list-group" style="color: #000000;">
        <li class="list-group-item active" style="font-size: large; text-transform: uppercase;"><strong>A: <xsl:value-of select="ReferencesBase/StartAddress/text()" /></strong></li>
		<xsl:for-each select="ReferencesBase/text">
			<li class="list-group-item" style="font-size: medium;"><xsl:value-of select="text()" /></li>
		</xsl:for-each>
		<li class="list-group-item active" style="font-size: large; text-transform: uppercase;"><strong>B: <xsl:value-of select="ReferencesBase/EndAddress/text()" /></strong></li>
	</ul>
	</xsl:template>
	
	
	</xsl:stylesheet>
