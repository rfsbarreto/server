<?xml version="1.0" encoding="ISO-8859-1"?>

   <xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	exclude-result-prefixes="xs xdt err fn">
		
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="/">
	<ReferencesBase>
		<StartAddress><xsl:value-of select="ReferencesBase/StartAddress/text()" /></StartAddress>
		<EndAddress><xsl:value-of select="ReferencesBase/EndAddress/text()"/></EndAddress>
		<!-- Guarda em uma vari�vel o valor do endereco final -->
		<xsl:variable name="varEndAddress" select="ReferencesBase/EndAddress/text()" />
		<!-- Guarda em uma vari�vel o valor do endereco inicial -->
		<xsl:variable name="varStartAddress" select="ReferencesBase/StartAddress/text()" />	
	<xsl:for-each select="ReferencesBase/Marker">											
				<!-- Guarda em uma vari�vel o valor do googleText da marca N -->
				<xsl:variable name="varGoogleText" select="GoogleText/text()" />
				
				<!-- Guarda em uma vari�vel o valor do orderId da marca N+1 -->
				<xsl:variable name="idProx" select="@orderId+1" />
				
				<!-- Guarda em uma vari�vel o valor do googleText da marca N+1 -->
				<xsl:variable name="varGoogleTextNext" select="/ReferencesBase/Marker[@orderId=$idProx]/GoogleText/text()" />					
		<xsl:choose>
			<xsl:when test="@context = 'begin'">
				<xsl:choose>
					<xsl:when test="count(Reference) > 0">
						<text>
						<xsl:attribute name="ref">1</xsl:attribute>
						<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
						<xsl:attribute name="rua"><xsl:value-of select="Reference/address/text()" /></xsl:attribute>
						<xsl:attribute name="poi"><xsl:value-of select="Reference/name/text()" /></xsl:attribute>
						Siga em dire��o do 
						<xsl:text>&lt;b&gt;</xsl:text>
						<xsl:value-of select="Reference/name/text()" /> 
						<xsl:text>&lt;/b&gt;</xsl:text>
						na 
						<xsl:text>&lt;p&gt;</xsl:text>
						<xsl:value-of select="Reference/address/text()" />
						<xsl:text>&lt;/p&gt;</xsl:text>
						</text>
					</xsl:when>
					<xsl:otherwise>
						<text>
						<xsl:attribute name="ref">0</xsl:attribute>
						<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
						<xsl:attribute name="rua"><xsl:value-of select="substring-before($varStartAddress,'-')" /></xsl:attribute>
						<xsl:value-of select="GoogleText/text()" />
						</text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@context = 'final'">
				<xsl:choose>
					<xsl:when test="count(Reference) > 0">
						<text>
						<xsl:attribute name="ref">1</xsl:attribute>
						<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
						<xsl:attribute name="rua"><xsl:value-of select="substring-before($varEndAddress,'-')" /></xsl:attribute>
						<xsl:attribute name="poi"><xsl:value-of select="Reference/name/text()" /></xsl:attribute>
						Chegue no destino que fica pr�ximo a 
						<xsl:text>&lt;b&gt;</xsl:text>
						<xsl:value-of select="Reference/name/text()" />
						<xsl:text>&lt;/b&gt;</xsl:text>
						na 
						<xsl:text>&lt;p&gt;</xsl:text>
						<xsl:value-of select="substring-before($varEndAddress,'-')" />
						<xsl:text>&lt;/p&gt;</xsl:text>
						</text>
					</xsl:when>
					<xsl:otherwise>
						<text>
						<xsl:attribute name="ref">0</xsl:attribute>
						<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
						<xsl:attribute name="rua"><xsl:value-of select="substring-before($varEndAddress,'-')" /></xsl:attribute>
						Chegue no destino na 
						<xsl:text>&lt;p&gt;</xsl:text>
						<xsl:value-of select="substring-before($varEndAddress,'-')" />
						<xsl:text>&lt;/p&gt;</xsl:text>
						</text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@context = 'corner'">
				<xsl:choose>
					<xsl:when test="$varGoogleText = $varGoogleTextNext">
						
					</xsl:when>
					<xsl:when test="count(Reference) > 0">
						<text>
						<xsl:attribute name="ref">1</xsl:attribute>
						<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
						<xsl:attribute name="rua"><xsl:value-of select="Reference/address/text()" /></xsl:attribute>
						<xsl:attribute name="poi"><xsl:value-of select="Reference/name/text()" /></xsl:attribute>						
						<xsl:value-of select="GoogleText/text()" />
						</text>
					</xsl:when>
					<xsl:otherwise>
						<text>
						<xsl:attribute name="ref">0</xsl:attribute>
						<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
						<xsl:value-of select="GoogleText/text()" /></text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="@context = 'normal'" >
				<xsl:variable name="contextonormal" select="normalContext/text()" />
				<xsl:choose>
					<xsl:when test="$contextonormal = 'normal'">
						<xsl:choose>
							<xsl:when test="count(Reference) > 0">
								<text>
								<xsl:attribute name="ref">1</xsl:attribute>
								<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
								<xsl:attribute name="rua"><xsl:value-of select="Reference/address/text()" /></xsl:attribute>
								<xsl:attribute name="poi"><xsl:value-of select="Reference/name/text()" /></xsl:attribute>
								Passe por 
								<xsl:text>&lt;b&gt;</xsl:text>
								<xsl:value-of select="Reference/name/text()" />
								<xsl:text>&lt;/b&gt;</xsl:text> 
								na 
								<xsl:text>&lt;p&gt;</xsl:text>
								<xsl:value-of select="Reference/address/text()" />
								<xsl:text>&lt;/p&gt;</xsl:text>
								</text>
							</xsl:when>
							<xsl:otherwise>
								<text>
								<xsl:attribute name="ref">0</xsl:attribute>
								<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
								<xsl:attribute name="rua"><xsl:value-of select="address/text()" /></xsl:attribute>
								Siga pela 
								<xsl:text>&lt;p&gt;</xsl:text>
								<xsl:value-of select="address/text()" />
								<xsl:text>&lt;/p&gt;</xsl:text>
								</text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$contextonormal = 'longpath'">
						<xsl:choose>
							<xsl:when test="count(Reference) > 0">
									<text>
									<xsl:attribute name="ref">1</xsl:attribute>
									<xsl:attribute name="context">longpath</xsl:attribute>
									<xsl:attribute name="rua"><xsl:value-of select="Reference/address/text()" /></xsl:attribute>
									<xsl:attribute name="poi"><xsl:value-of select="Reference/name/text()" /></xsl:attribute>
									Continue pela 
									<xsl:text>&lt;p&gt;</xsl:text>
									<xsl:value-of select="Reference/address/text()" /> 
									<xsl:text>&lt;/p&gt;</xsl:text> 
									passando por 
									<xsl:text>&lt;b&gt;</xsl:text>
									<xsl:value-of select="Reference/name/text()" />	
									<xsl:text>&lt;/b&gt;</xsl:text>
									</text>
							</xsl:when>
							<xsl:otherwise>
								<text>
								<xsl:attribute name="ref">0</xsl:attribute>
								<xsl:attribute name="context">longpath</xsl:attribute>
								<xsl:attribute name="rua"><xsl:value-of select="address/text()" /></xsl:attribute>
								Continue pela 
								<xsl:text>&lt;p&gt;</xsl:text>
								<xsl:value-of select="address/text()" />
								<xsl:text>&lt;/p&gt;</xsl:text>
								</text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
		</xsl:for-each>
		</ReferencesBase>
	</xsl:template>
	
	<!--
	<xsl:template name="BEGIN-TEMPLATE" match="/">
		
	</xsl:template>
	 
	<xsl:template name="FINAL-TEMPLATE" match="/">
		
	</xsl:template>
	
	<xsl:template name="CORNER-TEMPLATE" match="/">
		<text>Vire na <xsl:value-of select="Reference/addres/text()" /> pr�ximo a <xsl:value-of select="Reference/name/text()" /></text>
	</xsl:template>
	
	<xsl:template name="LONGPATH-TEMPLATE" match="/">
		<text>
			Continue na <xsl:value-of select="Reference/address/text()" /> passando por
			<xsl:call-template name='APPLY-LONGPATH-TEMPLATE' />
		</text>
	</xsl:template>
	
	<xsl:template name="NORMAL-TEMPLATE" match="/">
		<text>
			Passe por <xsl:value-of select="Reference/name/text()" /> na <xsl:value-of select="Reference/address/text()" />
		</text>
	</xsl:template>
	
	
	<xsl:template name="APPLY-LONGPATH-TEMPLATE" match="/">
		<xsl:for-each select="">
		
		
			DESCREVER O TEMPLATE DE CAMINHOS LONGOS, PARA EVITAR MUITOS ... CONTINUE PELA, CONTINUE PELA ... 
				
		</xsl:for-each>
	</xsl:template>
	
	-->
	
	</xsl:stylesheet>
	
	
