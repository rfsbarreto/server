<?xml version="1.0" encoding="ISO-8859-1"?>

   <xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	exclude-result-prefixes="xs xdt err fn">
		
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="/">
	<ReferencesBase>
		<StartAddress><xsl:value-of select="ReferencesBase/StartAddress/text()" /></StartAddress>
		<EndAddress><xsl:value-of select="ReferencesBase/EndAddress/text()"/></EndAddress>
	<xsl:for-each select="ReferencesBase/text">
	
		<xsl:variable name="posicao" select="position()+1" />
		<xsl:variable name="posicao2" select="position()" />
		<xsl:variable name="textN" select="/ReferencesBase/text[$posicao]/text()" />
		<xsl:variable name="text" select="/ReferencesBase/text[$posicao2]/text()" />
		
		<!-- <teste><xsl:value-of select="$textP" /></teste> -->
		
		<xsl:choose>
			<xsl:when test="$textN != $text">
				<text><xsl:value-of select="$text" /></text>
			</xsl:when>
			<xsl:when test="$posicao2 = last()">
				<text><xsl:value-of select="$text" /></text>
			</xsl:when>
		</xsl:choose>
		
		
	</xsl:for-each>
	
	</ReferencesBase>
	</xsl:template>
	
	</xsl:stylesheet>
