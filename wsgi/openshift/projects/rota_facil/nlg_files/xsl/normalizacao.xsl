<?xml version="1.0" encoding="ISO-8859-1"?>

   <xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xdt="http://www.w3.org/2005/xpath-datatypes"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	>

	<!-- Define o m�todo de sa�da -->
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="/">
    
	<ReferencesBase>
		<StartAddress><xsl:value-of select="ReferencesBase/StartAddress/text()" /></StartAddress>
		<EndAddress><xsl:value-of select="ReferencesBase/EndAddress/text()"/></EndAddress>
	<xsl:for-each select="ReferencesBase/Marker">
		<Marker>
		<xsl:attribute name="orderId"><xsl:value-of select="@orderId" /></xsl:attribute>
		<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
		<xsl:attribute name="context"><xsl:value-of select="@context" /></xsl:attribute>
		<xsl:attribute name="lat"><xsl:value-of select="@lat" /></xsl:attribute>
		<xsl:attribute name="long"><xsl:value-of select="@long" /></xsl:attribute>
				
			<xsl:if test="@type = '0'">
				<GoogleText><xsl:value-of select="GoogleText" /></GoogleText>
			</xsl:if>
			
			<address><xsl:value-of select="address" /></address>
			
			<!-- Guarda em uma vari�vel o valor do contexto da marca N -->
			<xsl:variable name="varContext" select="@context" />
			
			<!-- Guarda em uma vari�vel o valor do endere�o da marca N -->
			<xsl:variable name="varAddress" select="address" />
			
			<xsl:variable name="posicao" select="position()" />
			
			
			
			<!-- Guarda em uma vari�vel os valores dos endere�os das marcas N-1 e N+1 -->
				
				<xsl:variable name="varAddressP" select="/ReferencesBase/Marker[$posicao=@orderId+2]/address" />
				
			<!-- <xsl:variable name="varAddressN" select="/ReferencesBase/Marker[position()+1]/address" /> -->
				
			<!--
				Seleciona a refer�ncia adequada para cada um dos pontos de acordo com o contexto
				no qual eles se encaixam e as restri��es aplicadas a este contexto.
			-->
			<xsl:choose>
				<xsl:when test="$varContext = 'begin'">
					<xsl:for-each select="Reference">
					<xsl:sort select="@distanceToNext" data-type="number" />
					
						<xsl:if test="$varAddress = address">
						
							<xsl:if test="position() = '1'">
								<Reference>	
									<xsl:attribute name="lat"><xsl:value-of select="@lat" /></xsl:attribute>
									<xsl:attribute name="long"><xsl:value-of select="@long" /></xsl:attribute>
									<xsl:attribute name="distance"><xsl:value-of select="@distance" /></xsl:attribute>							
									<name><xsl:value-of select="name" /></name>
									<description><xsl:value-of select="description" /></description>
									<address><xsl:value-of select="address" /></address>
								</Reference>
							</xsl:if>
						
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$varContext = 'final'">
					<xsl:for-each select="Reference">				
					<xsl:sort select="@distanceToPrev" data-type="number" />
					
						<xsl:if test="$varAddress = address">
						
							<xsl:if test="position() = '1'">
								<Reference>	
									<xsl:attribute name="lat"><xsl:value-of select="@lat" /></xsl:attribute>
									<xsl:attribute name="long"><xsl:value-of select="@long" /></xsl:attribute>
									<xsl:attribute name="distance"><xsl:value-of select="@distance" /></xsl:attribute>							
									<name><xsl:value-of select="name" /></name>
									<description><xsl:value-of select="description" /></description>
									<!-- <address><xsl:value-of select="address" /></address> -->
									<address><xsl:value-of select="$varAddress" /></address>
								</Reference>
							</xsl:if>
						
						</xsl:if>
						
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$varContext = 'normal'">
					<!-- 
						
						O objetivo � agrupar as refer�ncias das marcas N-1 e N+1 em N, desde que as
						marcas N-1, N e N+1 possuam o mesmo endere�o.
						
					-->
					<xsl:choose>
						<xsl:when test="($varAddressP = $varAddress)">
							<normalContext>longpath</normalContext>
						</xsl:when>
						<xsl:otherwise>
							<normalContext>normal</normalContext>
						</xsl:otherwise>
					</xsl:choose>	
					<xsl:for-each select="Reference">				
					<xsl:sort select="@distance" data-type="number" />
					
						<xsl:if test="$varAddress = address">
						
							<!-- <xsl:if test="position() = '1'"> -->
								<Reference>	
									<xsl:attribute name="lat"><xsl:value-of select="@lat" /></xsl:attribute>
									<xsl:attribute name="long"><xsl:value-of select="@long" /></xsl:attribute>
									<xsl:attribute name="distance"><xsl:value-of select="@distance" /></xsl:attribute>							
									<name><xsl:value-of select="name" /></name>
									<description><xsl:value-of select="description" /></description>
									<address><xsl:value-of select="address" /></address>
								</Reference>
							<!-- </xsl:if> -->
						
						</xsl:if>
						
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="Reference">
					<xsl:sort select="@distance" data-type="number" />
						<xsl:if test="$varAddress = address">
						
							<xsl:if test="position() = '1'">
								<Reference>	
									<xsl:attribute name="lat"><xsl:value-of select="@lat" /></xsl:attribute>
									<xsl:attribute name="long"><xsl:value-of select="@long" /></xsl:attribute>
									<xsl:attribute name="distance"><xsl:value-of select="@distance" /></xsl:attribute>							
									<name><xsl:value-of select="name" /></name>
									<description><xsl:value-of select="description" /></description>
									<address><xsl:value-of select="$varAddressP" /></address>
								</Reference>
							</xsl:if>
						
						</xsl:if>
						
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
			
		</Marker>
	</xsl:for-each>
	</ReferencesBase>

	</xsl:template>
	
	</xsl:stylesheet>
