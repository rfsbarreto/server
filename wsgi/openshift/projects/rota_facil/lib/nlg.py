#coding=utf-8
from map import *
from lxml import etree
from random import randint


import os.path

if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    PROJECT_PATH = os.environ['OPENSHIFT_REPO_DIR']+"wsgi/openshift"
else:
    PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

#Files to apply the templates
NORMALIZED_FILE = PROJECT_PATH+'/projects/rota_facil/nlg_files/xsl/normalizacao.xsl'
TEMPLATE_FILE = PROJECT_PATH+'/projects/rota_facil/nlg_files/xsl/templates.xsl'
TREATMENT_FILE = PROJECT_PATH+'/projects/rota_facil/nlg_files/xsl/tratamento.xsl'
HTML_FILE = PROJECT_PATH+'/projects/rota_facil/nlg_files/xsl/html.xsl'
REFERENCES_FILE = PROJECT_PATH+'/projects/rota_facil/nlg_files/xsl/referencesMarker.xsl'

BEGIN_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/begin.txt'
FINAL_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/final.txt'
NORMAL_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/normal.txt'
CORNER_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/corner.txt'
CORNERLEFT_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/cornerLeft.txt'
CORNERIGHT_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/cornerRight.txt'
LONGPATH_TXT = PROJECT_PATH+'/projects/rota_facil/nlg_files/text_templates/longpath.txt'

references_markers = []


def get_text(str_to, str_from, initial_point, points_with_context):

    referencesBase = etree.Element('ReferencesBase')
    doc = etree.ElementTree(referencesBase)

    start = etree.SubElement(referencesBase, 'StartAddress')
    start.text = str_from

    end = etree.SubElement(referencesBase, 'EndAddress')
    end.text = str_to

    references = []

    usedReferences = []

    count = 0

    for item in points_with_context:
        number = str(count)
        count += 1

        lat, lng = item['point']

        marker = etree.SubElement(
            referencesBase,
            'Marker',
            orderId=number,
            type=str(item['type']),
            context=item['context'],
            lat=str(lat),
            long=str(lng),
            distanceToNext=str(item['distanceNext'])
        )

        if item['google_text'] != 'none':
            googleMarker = etree.SubElement(marker, 'GoogleText')
            googleMarker.text = item['google_text']

        for r in item['references']:
            point = r['point']
            if point not in usedReferences:
                usedReferences.append(point)
                references.append(r)

        for ref in references:
            latref, lngref = ref['point']

            if item['context'] == 'begin':
                refmarker = etree.SubElement(
                    marker,
                    'Reference',
                    id=str(ref['id']),
                    lat=str(latref),
                    long=str(lngref),
                    distance=str(ref['distance']),
                    distanceToNext=str(ref['distanceNext'])
                )
            elif item['context'] == 'final':
                refmarker = etree.SubElement(
                    marker,
                    'Reference',
                    id=str(ref['id']),
                    lat=str(latref),
                    long=str(lngref),
                    distance=str(ref['distance']),
                    distranceToPrev=str(ref['distancePrev'])
                )
            else:
                refmarker = etree.SubElement(
                    marker,
                    'Reference',
                    id=str(ref['id']),
                    lat=str(latref),
                    long=str(lngref),
                    distance=str(ref['distance']),
                    distanceToPrev=str(ref['distancePrev']),
                    distanceToNext=str(ref['distanceNext'])
                )

            nameRef = etree.SubElement(refmarker, 'name')
            nameRef.text = ref['name']

            descriptionRef = etree.SubElement(refmarker, 'description')
            descriptionRef.text = ''

            addressR_ = latlng_to_address(latref, lngref)

            if addressR_['status'] == "OK":

                addressR = addressR_['address'].split(",")[0]
                addressRef = etree.SubElement(refmarker, 'address')
                addressRef.text = addressR

        references = []

        # TODO: tratar possíveis erros neste ponto
        addressM_ = latlng_to_address(lat, lng)

        if addressM_['status'] == "OK":
            addressM = addressM_['address'].split(",")[0]
            addressMarker = etree.SubElement(marker, 'address')
            addressMarker.text = addressM

    html_tree = generate_text(doc)

    html_text_ = etree.tostring(html_tree)

    html_text_ = html_text_.replace("&lt;", "<")
    html_text_ = html_text_.replace("&gt;", ">")

    return html_text_


def generate_text(file):

    #normalized_ = normalized_text(file)

    template_ = template_text(file)

    new_template_ = use_new_templates(template_)

    treatment_ = treatment_text(new_template_)

    html_ = html_text(treatment_)

    #print html_

    #reference_ = reference_text(normalized_)

    #set_references_markers(reference_)

    return html_


def normalized_text(file):

    xslt_root = etree.parse(NORMALIZED_FILE)

    transform = etree.XSLT(xslt_root)

    result_tree = transform(file)

    return result_tree


def template_text(normalized_):

    xslt_root = etree.parse(TEMPLATE_FILE)

    transform = etree.XSLT(xslt_root)

    result_tree = transform(normalized_)

    return result_tree


def treatment_text(template_):

    xslt_root = etree.parse(TREATMENT_FILE)

    transform = etree.XSLT(xslt_root)

    result_tree = transform(template_)

    return result_tree


def html_text(treatment_):

    xslt_root = etree.parse(HTML_FILE)

    transform = etree.XSLT(xslt_root)

    result_tree = transform(treatment_)

    return result_tree


def reference_text(normalized_):

    xslt_root = etree.parse(REFERENCES_FILE)

    transform = etree.XSLT(xslt_root)

    result_tree = transform(normalized_)

    return result_tree


def set_references_markers(reference_):

    root = reference_.getroot()

    global references_markers

    references_markers = []

    for element in root.iter("Reference"):
        name = element[0].text
        lat = element[1].text
        lng = element[2].text

        reference = {
            'name': name,
            'lat': lat,
            'lng': lng
        }

        if reference not in references_markers:
            references_markers.append(reference)


def get_markers():
    global references_markers
    return references_markers


'''
Dado o contexto do template, retorna uma lista
com todas as frases possiveis para aquele template
'''


def get_template_list (context, poi):

    result_list = []

    if context == 'googleText':
        return -1

    if poi == '-1':
        result_list = get_template_list_without_poi(get_file_name(context))
    else:
        result_list = get_template_list_with_poi(get_file_name(context))

    return result_list

'''
Dado que o contexto e corner, descobre se este
e Left ou Right
'''


def context_corner(context, text):

    if text.find('esquerda') != -1:
        return 'cornerLeft'
    elif text.find('direita') != -1:
        return 'cornerRight'
    else:
        return 'corner'


'''
Retorna o caminho para o arquivo
referente ao contexto dado
'''


def get_file_name(context):

    if context == 'begin':
        return BEGIN_TXT
    elif context == 'final':
        return FINAL_TXT
    elif context == 'longpath':
        return LONGPATH_TXT
    elif context == 'corner':
        return CORNER_TXT
    elif context == 'cornerLeft':
        return CORNERLEFT_TXT
    elif context == 'cornerRight':
        return CORNERIGHT_TXT
    elif context == 'normal':
        return NORMAL_TXT
    else:
        return -1


'''
Dado o nome do arquivo, retorna todos os
templates possiveis para este usando POI
na descricao
'''


def get_template_list_with_poi(file_name):

    template_list = []

    if file_name != -1:
        file = open(file_name)
        for line in file:
            if line.find('POI') != -1:
                template_list.append(line.strip())
        file.close()

    return template_list



'''
Dado o nome do arquivo, retorna todos os
templates possiveis para este sem POI
na descricao
'''


def get_template_list_without_poi(file_name):
    template_list = []

    if file_name != -1:
        file = open(file_name)
        for line in file:
            if line.find('POI') == -1:
                template_list.append(line.strip())
        file.close()

    return template_list


def use_new_templates(template_):

    root = template_.getroot()

    for element in root.iter("text"):
        ref = element.attrib['ref']
        context = element.attrib['context']

        if ref == '1':
            if context == 'corner':
                text = ' '.join(element.text.split())
                context = context_corner(context, text)
                rua = text[text.rfind('<b>'):text.rfind('</b>')+4]
                poi = '<b>' + element.attrib['poi'] + '</b>'
            else:
                rua = '<b>' + element.attrib['rua'] + '</b>'
                poi = '<b>' + element.attrib['poi'] + '</b>'
        else:
            if context == 'begin':
                text = ' '.join(element.text.split())
                context = 'googleText'
                poi = '-1'
                rua = 'google text do begin'
            elif context == 'corner':
                text = ' '.join(element.text.split())
                context = context_corner(context, text)
                rua = text[text.rfind('<b>'):text.rfind('</b>')+4]
                poi = '-1'
            else:
                rua = '<b>' + element.attrib['rua'] + '</b>'
                poi = '-1'

        if context == 'googleText':
            element.text = text

        phrases = get_template_list(context,poi)

        if phrases != -1:
            index = randint(0,len(phrases)-1)
            new_phrase = phrases[index].replace('RUA',rua).replace('POI',poi)

            if new_phrase != '':
                element.text = new_phrase

    return template_

