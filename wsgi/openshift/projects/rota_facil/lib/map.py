#coding=utf-8

import ast
import math
import urllib
import urllib2
import logging

import json
from django.core.exceptions import ObjectDoesNotExist
from projects.rota_facil.models import LatLngCaching, GeocodeCaching, DirectionsCaching
from unicodedata import normalize
from operator import itemgetter


#RotaFacil Config Variables

#Google Places Paramters
RADIUS_SEARCH = 25 #get all the places inside the radius

#Google API URL
_DIRECTION_QUERY_URL_ = 'https://maps.googleapis.com/maps/api/directions/json?'
_GEOCODE_QUERY_URL_ = 'https://maps.googleapis.com/maps/api/geocode/json?'
#_PLACE_QUERY_URL_ = 'https://maps.googleapis.com/maps/api/place/search/json?'
_PLACE_QUERY_URL_ = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'

#ERRO REQUEST
STATUS_OK = 'OK'
DIRECTIONS_STATUS_NOT_FOUND = 'NOT_FOUND'
DIRECTIONS_STATUS_ZERO_RESULTS = 'ZERO_RESULTS'
DIRECTIONS_STATUS_MAX_WAYPOINTS_EXCEEDED = 'MAX_WAYPOINTS_EXCEEDED'
DIRECTIONS_STATUS_INVALID_REQUEST = 'INVALID_REQUEST'
DIRECTIONS_STATUS_OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT'
DIRECTIONS_STATUS_REQUEST_DENIED = 'REQUEST_DENIED'
DIRECTIONS_STATUS_UNKNOWN_ERROR = 'UNKNOWN_ERROR'

PLACES_STATUS_ZERO_RESULTS = 'ZERO_RESULTS'
PLACES_STATUS_OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT'
PLACES_REQUEST_DENIED = 'REQUEST_DENIED'
PLACES_INVALID_REQUEST = 'INVALID_REQUEST'

logger = logging.getLogger(__name__)

places_list = []

"""
    Implementa o Geocode do Google Maps

    Endereco ----> Latitude/Longitude

"""


def geocode(query, sensor='false',oe='utf8',ll='',spn='',gl=''):

    params = {
        'address': query,
        'sensor': sensor,
        'output': json,
        'key': 'AIzaSyDlOR05zTBBMc0UgPYKcoVuaC03bGDdDPo',
        'oe': oe,
        'll': ll,
        'spn': spn,
        'gl': gl,
    }

    url, response = fetch_json(_GEOCODE_QUERY_URL_, params=params)
    status_code = response['status']
    if status_code != STATUS_OK:
        print "Erro: " + status_code

    return response


"""
    Implementa o Geocode Reverse do Google Maps

    Longitude/Latitude ----> Endereco

"""


def reverse_geocode(lat, lng, sensor='false', oe='utf8', ll='', spn='', gl=''):

    query = "%f, %f" % tuple((float(lat), float(lng)))

    params = {
        'latlng': query,
        'sensor': sensor,
        'output': json,
        'key': 'AIzaSyDlOR05zTBBMc0UgPYKcoVuaC03bGDdDPo',
        'oe': oe,
        'll': ll,
        'spn': spn,
        'gl': gl,
    }

    url, response = fetch_json(_GEOCODE_QUERY_URL_, params=params)

    #print response

    status_code = response['status']
    if status_code != STATUS_OK:
        print "Error on reverse_geocode"
        print status_code

    return response


def latlng_to_address(lat, lng):

    address_result = reverse_geocode(lat, lng)

    if address_result['status'] == "OK":
        address = address_result['results'][0]['formatted_address']
        #q_save = GeocodeCaching(latitude=lat, longitude=lng, address=address);
        #q_save.save()

        dic_address = {
            'status': "OK",
            'address': address
        }

    else:

        dic_address = {
            'status': address_result['status'],
            'address': ""
        }

    '''
    try:
        q = GeocodeCaching.objects.filter(latitude=lat, longitude=lng)[:1].get()
        address = q.get_address()

        dic_address = {
            'status': "OK",
            'address': address
        }

    except ObjectDoesNotExist:

        address_result = reverse_geocode(lat, lng)

        if address_result['status'] == "OK":
            address = reverse_geocode(lat, lng)['results'][0]['formatted_address']
            q_save = GeocodeCaching(latitude=lat, longitude=lng, address=address);
            q_save.save()

            dic_address = {
                'status': "OK",
                'address': address
            }

        else:

            dic_address = {
                'status': address_result['status'],
                'address': ""
            }

    #ad_split = address.split(',')
    '''
    return dic_address


def address_to_latlng(add):
    try:
        q = LatLngCaching.objects.filter(address=add)[:1].get()
        point = q.get_point();
    except ObjectDoesNotExist:
        point = geocode(add)['results'][0]['geometry']['location'];
        q_save = LatLngCaching(address=add,latitude=point['lat'],longitude=point['lng']);
        q_save.save()

    return point


def get_directions_by_id(id_direction):
    q = DirectionsCaching.objects.get(id=id_direction)
    response = q.get_directions()
    response = ast.literal_eval(response)
    response['id'] = q.get_id()
    return response


def get_directions(origin, destination):


    params = {
        'origin': remover_acentos(origin.encode('utf-8')).lower(),
        'destination': remover_acentos(destination.encode('utf-8')).lower(),
        'mode': 'driving',
        'output': 'json',
        'alternatives': 'true',
        'sensor': 'false',
        'language': 'pt-br',
        'key': 'AIzaSyDlOR05zTBBMc0UgPYKcoVuaC03bGDdDPo',
    }

    url, response = fetch_json(_DIRECTION_QUERY_URL_, params=params)
    response['id'] = -1

    return response
    '''
    try:
        q = DirectionsCaching.objects.get(origin=origin, destination=destination)

        response = q.get_directions()
        response = ast.literal_eval(response)
        response['id'] = q.get_id()

        print "Aqui"

    except ObjectDoesNotExist:

        url, response = fetch_json(_DIRECTION_QUERY_URL_, params=params)

        status_code = response['status']

        if status_code == STATUS_OK:
            q_save = DirectionsCaching(origin=origin, destination=destination, directions=response)
            q_save.save()
            q_after_save = DirectionsCaching.objects.get(origin=origin, destination=destination)
            response['id'] = q_after_save.get_id()

    except BaseException as e:

        print e.message
        response = {'status': "ERROR", 'message': "AN EXCEPTION OCURRED"}

    return response
    '''

#TODO: verificar o impacto da alteração realizada pelo google descrita em: http://googlegeodevelopers.blogspot.com.br/2016/02/changes-and-quality-improvements-in_16.html
###ACTUAL_KEY: AIzaSyDlOR05zTBBMc0UgPYKcoVuaC03bGDdDPo
#FORMER_KEY: AIzaSyDXvhbVPBs8gbOoMBXTVHdKUMptx_St9nk


def get_places(lat, lng, radius):

    location = "%f,%f" % tuple((lat,lng))

    params = {
        'location': location,
        'radius': radius,
        'language': 'pt-br',
        'sensor': 'false',
        'key': 'AIzaSyDlOR05zTBBMc0UgPYKcoVuaC03bGDdDPo',
        'types': 'establishment'
    }

    url, response = fetch_json(_PLACE_QUERY_URL_, params=params)

    status = response['status']

    return_value = response

    if status == STATUS_OK:
        message = ""
    elif status == PLACES_INVALID_REQUEST:
        message = "INVALID REQUEST"
    elif status == PLACES_STATUS_OVER_QUERY_LIMIT:
        message = "OVER QUERY LIMIT"
    elif status == PLACES_REQUEST_DENIED:
        message = "REQUEST DENIED"
    elif status == PLACES_STATUS_ZERO_RESULTS:
        message = "ZERO RESULTS"
    else:
        message = "AN ERROR OCURRED"

    return return_value, message


def fetch_json(query_url, params={}, headers={}):
    encoded_params = urllib.urlencode(params)
    url = query_url + encoded_params

    request = urllib2.Request(url, headers=headers)
    response = urllib2.urlopen(request)

    return (url, json.load(response))


def get_ordem_lat_lng(ordemCerta,ordemAtual):

    lat = ordemCerta[ordemCerta.rfind('(')+1:ordemCerta.rfind(',')]
    lng = ordemCerta[ordemCerta.rfind(',')+2:ordemCerta.rfind(')')]

    if (float(lat) == ordemAtual[0]) & (float(lng) == ordemAtual[1]):
        return True
    elif (float(lat) == ordemAtual[1]) & (float(lng) == ordemAtual[0]):
        return False
    else:
        return -1

def calc_distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 #km earth

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)

    a = (math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d


def get_final(points, address=True, number_references=1):

    places_list = []

    count = 0

    for p in points:

        references = p['references']

        number_references_ = number_references

        if number_references_ > len(references):
            number_references_ = len(references)

        sorted(references, key=itemgetter('distance'))

        if len(references) > 0:
            point_marker = p['point']

            if address:
                address_marker = latlng_to_address(point_marker[0], point_marker[1])['address'].split(",")[0]

            p['references'] = []

            for n in range(0, number_references_):

                point_reference = references[n]['point']


                if address:
                    address_reference = latlng_to_address(point_reference[0], point_reference[1])['address'].split(",")[0]
                    if address_marker == address_reference:
                        p['references'].append(references[n])
                        places_list.append(references[n])
                else:
                    p['references'] = [references[n]]
                    places_list.append(references[n])


    return points, places_list

def get_final_bk(points, address=True):

    places_list = []

    count = 0

    for p in points:
        references = p['references']

        sorted(references, key=itemgetter('distance'))

        if len(references) > 0:
            point_marker = p['point']
            point_reference = references[0]['point']
            address_marker = latlng_to_address(point_marker[0], point_marker[1])['address'].split(",")[0]
            address_reference = latlng_to_address(point_reference[0], point_reference[1])['address'].split(",")[0]

            if address:
                if address_marker == address_reference:

                    p['references'] = [references[0]]

                    #if search_place(references[0]['name'], places_list) == False:
                    places_list.append(references[0])

                    #print references[0]['name']
                else:
                    p['references'] = []
            else:
                p['references'] = [references[0]]
                places_list.append(references[0])

    return points, places_list

def search_place(name, places_list):
    number = [element for element in places_list if element['name'] == name]
    if number != 0:
        return True
    else:
        return False


def get_places_list():
    return places_list



def set_contexts(points_with_references):

    count = 0

    for p in points_with_references:

        distanceToNext = 0

        if p['type'] == 'key':
            type = 0
        else:
            type = 1

        if count == 0:
            context = 'begin'
        elif count == len(points_with_references) - 1:
            context = 'final'
        else:
            if type == 0:
                keyword = p['google_text'].split(' ', 1)[0]
                if keyword == 'Vire' or keyword == 'Pegue' or keyword == 'Curva':
                    context = 'corner'
                else:
                    context = 'normal'
            else:
                context = 'normal'

        if context != 'final':
            distanceToNext = calc_distance(p['point'],points_with_references[count+1]['point'])

        p['type'] = type
        p['context'] = context
        p['distanceNext'] = distanceToNext

        referencesPoint = p['references']

        for r in referencesPoint:

            distanceRefNext = 0
            distanceRefPrev = 0

            if context == 'begin':
                distanceRefNext = calc_distance(r['point'], points_with_references[count+1]['point'])
            elif context == 'final':
                distanceRefPrev = calc_distance(r['point'], points_with_references[count-1]['point'])
            else:
                distanceRefPrev = calc_distance(r['point'], points_with_references[count-1]['point'])
                distanceRefNext = calc_distance(r['point'], points_with_references[count+1]['point'])

            r['distancePrev'] = distanceRefPrev
            r['distanceNext'] = distanceRefNext

        count += 1

    return points_with_references


def set_references_to_points(select_points, radius=25):



    for p in select_points:

        lat, lng = p['point']

        places, message = get_places(lat, lng, radius)

        status = places['status']

        references_list = []

        id = 0

        if status == STATUS_OK:

            list_places = places['results']

            for place in list_places:

                point_place = (place['geometry']['location']['lat'], place['geometry']['location']['lng'])
                point_route = p['point']

                d = calc_distance(point_place, point_route)

                #if d <= RADIUS_SEARCH / 100.0:
                reference = {
                    'id': id,
                    'name': place['name'],
                    'point': point_place,
                    'distance': d,
                }
                references_list.append(reference)
                id += 1
        else:
            references_list = []
            p['no_result_message'] = message

        p['status'] = status
        p['references'] = references_list


    return select_points


"""
    Seleciona os pontos que serao utilizados para determinar o conjuntos de referencias
    Entrada: todos os pontos da rota
             routa do texto do google
    Saida: conjunto de pontos selecionados

"""

# TODO: Revisar variáveis não utilizadas
def select_points(array_line, google_route):

    points_of_google = []
    key_points = []
    selected_points = []
    key_texts = []

    for step in google_route:

        tuple_ = (step['point']['lat'], step['point']['lng'])
        text_ = step['text']

        google_point = {
            'point': tuple_,
            'text': text_
        }

        points_of_google.append(google_point)

    count_key = 0

    for point in array_line:
        for gpoint in points_of_google:
            d = calc_distance(point, gpoint['point'])
            if d <= 0.001:
                key_points.append(point)
                key_texts.insert(count_key,gpoint['text'])
                count_key += 1

    p_act = array_line[0]
    p_prev = array_line[0]
    p_next = array_line[0]

    count_key = 0

    for point in array_line:
        d_from_origim = calc_distance(array_line[0], point)
        p_act = point

        if point in key_points:

            sp = {
                'google_text': key_texts[count_key],
                'point': point,
                'type': 'key',
            }

            if sp not in selected_points:
                selected_points.append(sp)

            p_prev = point
            count_key += 1

        else:
            d = calc_distance(p_act, p_prev)

            if d >= 0.3:
                sp = {
                    'google_text': 'none',
                    'point': point,
                    'type': 'not-key',
                }
                selected_points.append(sp)
                p_prev = point

    return selected_points


def remover_acentos(txt, codif='utf-8'):
        return normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')

def decode_line(encoded):

    """Decodes a polyline that was encoded using the Google Maps method.

    See http://code.google.com/apis/maps/documentation/polylinealgorithm.html

    This is a straightforward Python port of Mark McClure's JavaScript polyline decoder
    (http://facstaff.unca.edu/mcmcclur/GoogleMaps/EncodePolyline/decode.js)
    and Peter Chng's PHP polyline decode
    (http://unitstep.net/blog/2008/08/02/decoding-google-maps-encoded-polylines-using-php/)
    """

    encoded_len = len(encoded)
    index = 0
    array = []
    lat = 0
    lng = 0

    while index < encoded_len:

        b = 0
        shift = 0
        result = 0

        while True:
            b = ord(encoded[index]) - 63
            index = index + 1
            result |= (b & 0x1f) << shift
            shift += 5
            if b < 0x20:
                break

        dlat = ~(result >> 1) if result & 1 else result >> 1
        lat += dlat

        shift = 0
        result = 0

        while True:
            b = ord(encoded[index]) - 63
            index = index + 1
            result |= (b & 0x1f) << shift
            shift += 5
            if b < 0x20:
                break

        dlng = ~(result >> 1) if result & 1 else result >> 1
        lng += dlng

        array.append((lat * 1e-5, lng * 1e-5))

    return array
