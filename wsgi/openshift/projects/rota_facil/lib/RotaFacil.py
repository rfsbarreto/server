#coding=utf-8

import json
import urllib
import urllib2
import operator
from collections import Counter
from unicodedata import normalize


class RotaFacil:

    #Default config
    map_zoom = 12
    map_width = 530
    map_height = 500
    map_center = {'lat': -33.862828, 'lng': 151.216974}
    map_type = "normal"
    map_div_id = "map_rotafacil"
    mode = "rotafacil"
    has_auto_complete = False
    auto_complete_elements = []
    has_bound = True
    mark_all_points = []
    mark_points = False
    heatmap = False
    heatmap_points = []

    colors = ['#4B0082', '#FF0000', '#FF69B4', '#B22222', '#CD5C5C', '#FFFF00', '#FFE4E1', '#556B2F', '#808000', '#8FBC8F', '#FFC0CB', '#FF6347', '#F08080', '#FF4500', '#FFDEAD', '#00FF00', '#98FB98', '#ADFF2F', '#DEB887', '#FFF5EE', '#00FA9A', '#FF00FF', '#FFEFD5', '#FFEBCD', '#7FFF00', '#696969', '#000000', '#FFDAB9', '#00FF7F', '#7FFFD4', '#FFFFFF', '#FFA500', '#FFA07A', '#2F4F4F', '#A52A2A', '#FFFFF0', '#1E90FF', '#CD853F', '#7CFC00', '#D2691E', '#DC143C', '#228B22', '#6A5ACD', '#20B2AA', '#00FFFF', '#F5FFFA', '#C0C0C0', '#FAEBD7', '#BA55D3', '#87CEEB', '#808080', '#00CED1', '#DAA520', '#006400', '#FFFAF0', '#9400D3', '#A9A9A9', '#FFE4B5', '#8B4513', '#483D8B', '#87CEFA', '#FFB6C1', '#C71585', '#FFD700', '#FF1493', '#32CD32', '#8B008B', '#EEE8AA', '#DDA0DD', '#40E0D0', '#FAFAD2', '#B8860B', '#E6E6FA', '#800000', '#9ACD32', '#FAA460', '#D8BFD8', '#EE82EE', '#000080', '#FF00FF', '#D2B48C', '#BC8F8F', '#6B8E23', '#0000FF', '#ADD8E6', '#F8F8FF', '#F0FFF0', '#6495ED', '#FAF0E6', '#00008B', '#B0E0E6', '#2E8B57', '#BDB76B', '#FFFAFA', '#A0522D', '#0000CD', '#4169E1', '#E0FFFF', '#008000', '#9370DB', '#191970', '#FFF8DC', '#AFEEEE', '#FFE4C4', '#708090', '#008B8B', '#F0E68C', '#F5DEB3', '#008080', '#9932CC', '#00BFFF', '#FA8072', '#8B0000', '#4682B4', '#DB7093', '#778899', '#F0F8FF', '#90EE90', '#DA70D6', '#DCDCDC', '#3CB371', '#D3D3D3', '#48D1CC', '#FFFACD', '#5F9EA0', '#FFFFE0', '#FFF0F5', '#FF7F50', '#800080', '#00FFFF', '#F5F5F5', '#7B68EE', '#FF8C00', '#66CDAA', '#E9967A', '#F5F5DC', '#8A2BE2', '#F0FFFF', '#B0C4DE', '#FDF5E6']


    #Internal Variables
    has_directions = False
    directions = {}
    pointers = []
    
    google_maps_types = {
        'physical': 'TERRAIN',
        'normal': 'ROADMAP',
        'satellite': 'SATELLITE',
        'hybrid': 'HYBRID',
    }

    allow_modes = ['rotafacil', 'google']

    #Constructor
    def __init__(self, config={}):
        
        if(len(config) > 0): 
            self.loadConfig(config)
        
    def loadConfig(self, config):
        if 'map_zoom' in config:
            self.map_zoom = config['map_zoom']

        if 'map_width' in config:
            self.map_width = config['map_width']

        if 'map_height' in config:
            self.map_height = config['map_height']

        if 'map_center' in config:
            self.map_center = config['map_center']

        if 'map_type' in config: 
            if(config['map_type'] in self.google_maps_types.keys()):
                self.map_type = config['map_type']

        if 'map_div_id' in config:
            self.map_div_id = config['map_div_id']

        if 'has_auto_complete' in config:
            self.has_auto_complete = config['has_auto_complete']

        if 'auto_complete_elements' in config:
            self.auto_complete_elements = config['auto_complete_elements']

        if 'mode' in config:
            if config['mode'] in self.allow_modes:
                self.mode = config['mode']

        if 'heatmap' in config:
            self.heatmap = config['heatmap']
            
    def googleJS(self):
        return "<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDlOR05zTBBMc0UgPYKcoVuaC03bGDdDPo&sensor=false&libraries=places,visualization' type='text/javascript'></script>"
    
    def rotaFacilJS(self):
        #return "<script src='https://localhost:8080/static/js/rota_facil/map.js' type='text/javascript'></script>"
        return "<script src='http://go-goproject.rhcloud.com/static/js/rota_facil/map.js' type='text/javascript'></script>"
    
    def googleText(self):
        if self.has_directions:
            return self.directions['google_text']
        else: 
            return ""
    
    def rotaFacilText(self):
        if self.has_directions:
            return self.directions['rf_text']
        else:
            return ""
    
    def directionsStartAddress(self):
        if self.has_directions:
            return self.directions['start_address']
        else:
            return ""
        
    def directionsEndAddress(self):
        if self.has_directions:
            return self.directions['end_address']
        else:
            return ""

    def directionsPolyline(self):
        if self.has_directions:
            return self.directions['polyline']
        else:
            return ""

    def directionsId(self):
        if self.has_directions:
            return self.directions['id']
        else:
            return ""

    def getPlacesList(self):
        if self.has_directions:
            return self.directions['places_list']
        else:
            return ""
    
    def noJavaScript(self):
        return "<noscript><b>JavaScript must be enabled in order for you to use Google Maps.</b>  However, it seems JavaScript is either disabled or not supported by your browser.  To view Google Maps, enable JavaScript by changing your browser options, and then try again.</noscript>";
    

    def getDirectionsStatus(self):
        msg_return = {}
        msg_return['status'] = self.directions['status']
        msg_return['msg_error'] = self.directions['msg_error']

        return msg_return


    def getPointers(self):
        return self.pointers
        
    
    def addMarker(self, lat_, lng_, title_, type_):
        marker = {}
        marker['lat'] = str(lat_)
        marker['lng'] = str(lng_)
        marker['title'] = str(title_)
        marker['type'] = str(type_)

        self.pointers.append(marker)


    #Return a JavaScritp code to draw the map in web page.
    def drawMap(self):
        #Create divs to show the map
        js = "<div id='"+self.map_div_id+"' style='width: "+str(self.map_width)+"px; height: "+str(self.map_height)+"px'></div>\n"
        js += "<div id='"+self.map_div_id+"_foot'><small>&copy; 2010-2016 RotaFacil</small></div>\n"
        js += self.noJavaScript() + "\n"

        #Apply main configurations of the map
        js += "<script type='text/javascript'> " \
              "//<![CDATA[\n" \
              "var latlng = new google.maps.LatLng("+str(self.map_center['lat'])+","+str(self.map_center['lng'])+"); " \
              "var mapOptions = {" \
              " zoom: " + str(self.map_zoom)+ "," \
              " center: latlng, " \
              " disableDefaultUI: true," \
              " panControl: true," \
              " mapTypeControl: true," \
              " zoomControl: true," \
              " zoomControlOptions: {" \
              "    style: google.maps.ZoomControlStyle.SMALL " \
              " }," \
              " mapTypeId: google.maps.MapTypeId."+self.google_maps_types[self.map_type]+"" \
              "};\n" \
              "map = new google.maps.Map(document.getElementById('" + self.map_div_id + "'),mapOptions);" \
              "\n"


        #Insert at JS commands to use auto complete on search textbox
        if(self.has_auto_complete):
            js += "" \
                  "var defaultBounds = new google.maps.LatLngBounds(new google.maps.LatLng("+str(self.map_center['lat'])+","+str(self.map_center['lng'])+")); " \
                  "var options = { " \
                  " bounds: defaultBounds, " \
                  " types: ['establishment','geocode']" \
                  "};" \
                  "\n"

            for element in self.auto_complete_elements:
                js += "var input_id = document.getElementById('"+element+"');\n"
                js += "autocomplete = new google.maps.places.Autocomplete(input_id, options);\n"

            js += "\n"


        #Insert Markers
        for point in self.pointers:
            js += 'var point = new google.maps.LatLng(' + point['lat'] + ',' + point['lng'] + ');\n'
            js += 'var marker = createMarker(point,"' + point['title'] + '","' + point['type'] + '");\n'
            js += 'marker.setMap(map);\n'

        #MarkAllPoints
        if(self.mark_all_points):

            for p in self.mark_points:

                js += "var markCircle = new google.maps.Circle({ " \
                      " strokeColor: '" + self.colors[p['tipo']] + "' , " \
                      " strokeOpacity: 0.8, " \
                      " strokeWeight: 1, " \
                      " fillColor: '" + self.colors[p['tipo']] + "' , " \
                      " fillOpacity: 0.8, " \
                      " map: map, " \
                      " center: {lat: " + str(p['latitude']) + ", lng: " + str(p['longitude']) + "}, " \
                      " radius: 5" \
                      "});\n"

        #Insert Heatmap
        if(self.heatmap):

            dict_points = Counter(self.heatmap_points)

            maxvalue = max(dict_points.iteritems(), key=operator.itemgetter(1))[0]

            js += "points = ["

            for p in dict_points:
                js += "{location: new google.maps.LatLng(" + str(p[0]) + "," + str(p[1]) + "), weight: " + str(dict_points[p]) + "}, \n"

            js += "];"
            js += "\n"

            js += "heatmap = new google.maps.visualization.HeatmapLayer({ " \
                  "data: points," \
                  "radius: 15, " \
                  "maxIntensity: 100," \
                  "map: map" \
                  "});"
            js += "\n"
            js += "heatmap.setMap(map);\n"



        #Insert Directions
        if self.has_directions:

            for point in self.directions['polyline']:
                js += "var point = new google.maps.LatLng("+str(point[0])+","+str(point[1])+");\n"
                js += "pointsOfRouteLine.push(point);\n"

            js += "" \
                  "var route_path = new google.maps.Polyline({ " \
                  " path: pointsOfRouteLine, " \
                  " strokeColor: '#FF0000', " \
                  " strokeOpacity: 1.0, " \
                  " strokeWeight: 2" \
                  "});\n" \
                  "route_path.setMap(map);\n"

            if self.has_bound:
                ne_lat = self.directions['bounds']['northeast']['lat']
                ne_lng = self.directions['bounds']['northeast']['lng']

                sw_lat = self.directions['bounds']['southwest']['lat']
                sw_lng = self.directions['bounds']['southwest']['lng']

                js += "var neNew = new google.maps.LatLng(" + str(ne_lat) + "," + str(ne_lng) + ");\n"
                js += "var swNew = new google.maps.LatLng(" + str(sw_lat) + "," + str(sw_lng) + ");\n"
                js += "var boundsNew = new google.maps.LatLngBounds(swNew, neNew);\n"

                js += "\n"
                js += "map.fitBounds(boundsNew);\n"
                js += "map.setCenter(boundsNew.getCenter());\n"

        js += "//]]</script>"

        return js

    def setHeatMapPoints(self, points):
        self.heatmap_points = points

    def showPolyline(self, points):

        self.directions['polyline'] = points
        self.directions['status'] = "OK"


        total_polyline = len(self.directions['polyline'])

        point_A = self.directions['polyline'][0]
        point_B = self.directions['polyline'][total_polyline-1]

        self.pointers = []

        self.addMarker(point_A[0], point_A[1], "Start","A")
        self.addMarker(point_B[0], point_B[1], "End","B")

        self.has_directions = True
        self.has_bound = False


    def markAllPoints(self, points):

        self.mark_all_points = True
        self.mark_points = points
        self.map_zoom = 13

    def getAddress(self, point):

        #url_rotafacil = "http://localhost:8080/go/rota_facil/address/?lat="+urllib.quote_plus(str(point[0]))+"&lng="+urllib.quote_plus(str(point[1]))
        url_rotafacil = "https://go-goproject.rhcloud.com/go/rota_facil/address/?lat=" + urllib.quote_plus(str(point[0])) + "&lng=" + urllib.quote_plus(str(point[1]))

        url_result = urllib2.urlopen(url_rotafacil)

        data = json.load(url_result)

        return data


    #Return directions information
    def getDirections(self, from_, to_, id_=-1):

        self.pointers = []

        directions_from = self.remover_acentos(from_.encode('utf-8')).lower()
        directions_to = self.remover_acentos(to_.encode('utf-8')).lower()

        if(id_ == -1):
            if(self.mode == "rotafacil"):
                #url_rotafacil = "http://localhost:8080/go/rota_facil/request/?from="+urllib.quote_plus(directions_from)+"&to="+urllib.quote_plus(directions_to)+"&mode=rotafacil";

                url_rotafacil = "https://go-goproject.rhcloud.com/go/rota_facil/request/?from="+urllib.quote_plus(directions_from)+"&to="+urllib.quote_plus(directions_to)+"&mode=rotafacil"
            else:
                #url_rotafacil = "http://localhost:8080/go/rota_facil/request/?from="+urllib.quote_plus(directions_from)+"&to="+urllib.quote_plus(directions_to)+"&mode=google";
                url_rotafacil = "https://go-goproject.rhcloud.com/go/rota_facil/request/?from="+urllib.quote_plus(directions_from)+"&to="+urllib.quote_plus(directions_to)+"&mode=google"
        else:
            url_rotafacil = "http://go-goproject.rhcloud.com/go/rota_facil/request/?id="+str(id_)

        

        try:

            url_result = urllib2.urlopen(url_rotafacil)

            data = json.load(url_result)

            if data['status'] == "OK":

                self.directions['start_address'] = data['start_address']
                self.directions['end_address'] = data['end_address']
                self.directions['google_route'] = data['google_route']
                self.directions['polyline'] = data['polyline']
                self.directions['id'] = data['direction_id']
                self.directions['bounds'] = data['bounds']
                self.directions['rf_text'] = data['rf_text']
                self.directions['places_list'] = data['places_list']

                total_polyline = len(self.directions['polyline'])

                self.directions['google_text'] = ""

                for google_route in self.directions['google_route']:
                    self.directions['google_text'] += google_route['text'] + "<br />"

                point_A = self.directions['polyline'][0]
                point_B = self.directions['polyline'][total_polyline-1]

                start_address = self.remover_acentos(self.directions['start_address'].encode('utf-8'))
                end_address = self.remover_acentos(self.directions['end_address'].encode('utf-8'))

                self.addMarker(point_A[0], point_A[1], start_address,"A")
                self.addMarker(point_B[0], point_B[1], end_address,"B")

                if(self.mode == "rotafacil"):
                    places_list = self.directions['places_list']

                    for place in places_list:
                        name_place = place['name'].encode('utf-8')
                        lat_place = place['point'][0]
                        lng_place = place['point'][1]

                        self.addMarker(lat_place, lng_place, name_place, "R")

                self.directions['status'] = "OK"
                self.directions['msg_error'] = ""

                self.has_directions = True
                self.has_bound = True

            else:

                self.directions['status'] = data['status']
                self.directions['msg_error'] = data['message']

        except Exception as e:

            print "Aqui " + str(e)

            self.directions = {'status': "ERRO", 'msg_error': str(e)}

        return self.directions

    def remover_acentos(self,txt, codif='utf-8'):
        return normalize('NFKD', txt.decode(codif)).encode('ASCII', 'ignore')
