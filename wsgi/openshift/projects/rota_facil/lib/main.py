from api import RotaFacil


config = {
	'map_zoom':15,
	'map_width':600,
	'map_height':200,
	'map_type': 'satellite',
	'auto_complete_elements': ['txt1','txt2'],

}


rf = RotaFacil(config)

print rf.googleJS()
print rf.googleText()

Pointers = rf.getPointers()

for p in Pointers:
	print p


directions = rf.getDirections("Rua Frei Paulo, 1177, Aracaju, Sergipe","Rua Cristovao de Barros, Aracaju, Sergipe")
print rf.drawMap()
print rf.getDirectionsStatus()
print directions
