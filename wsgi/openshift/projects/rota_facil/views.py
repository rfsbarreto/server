# coding=utf-8

import json
import lib.map
import lib.nlg
import os.path
import register.lib.projects as r_projects

from django.shortcuts import render
from django.http import HttpResponse, StreamingHttpResponse
from lib.RotaFacil import RotaFacil
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    PROJECT_PATH = os.environ['OPENSHIFT_REPO_DIR']+"wsgi/openshift"
else:
    PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

URL_PAGE = PROJECT_PATH+"/projects/rota_facil/templates/project_page.html"
URL_WEB = PROJECT_PATH+"/templates/project.html"
PROJECT_NAME = "Rota Facil"
PROJECT_ID = "rota_facil"


def home(request):
    if not request.user.is_authenticated():
        return render(request, "index.html", {'status':'noactive'})
    else:
        args = {'projectid': PROJECT_ID, 'projectname': PROJECT_NAME, 'urlpage': URL_PAGE,
                'packages_dir': settings.PACKAGES_DIR,
                'list_user_projects': r_projects.get_users_of_project(PROJECT_ID)}

        return render(request, URL_WEB, args)


def get_address(request):
    str_lat = request.GET.get('lat', '')
    str_lng = request.GET.get('lng', '')

    if str_lat != '' and str_lng != '':

        dict_final = lib.map.latlng_to_address(str_lat, str_lng)

        return_value = json.dumps(dict_final)
    else:

        return_value = json.dumps({'status': "ERRO", 'message': "Missing Paramters"})

    return HttpResponse(return_value, content_type="text/javascript")


def get_route(request):
    str_from = request.GET.get('from', '')
    str_to = request.GET.get('to', '')
    str_id = request.GET.get('id', '')
    str_mode = request.GET.get('mode', '')
    str_radius = request.GET.get('radius', '')
    str_address = request.GET.get('address_compare', '')
    str_number = request.GET.get('number_references', '')

    if str_id != '':
        directions = lib.map.get_directions_by_id(str_id)
    else:
        directions = lib.map.get_directions(str_from, str_to)

    status_code = directions['status']

    if status_code == 'OK':

        polyline = directions['routes'][0]['overview_polyline']['points']
        bounds = directions['routes'][0]['bounds']
        route_text = directions['routes'][0]['legs'][0]['steps']
        start_address = directions['routes'][0]['legs'][0]['start_address']
        end_address = directions['routes'][0]['legs'][0]['end_address']
        direction_id = directions['id']

        array_line = lib.map.decode_line(polyline)

        google_route = []

        for text in route_text:

            point_route = {
                'point': text['start_location'],
                'text': text['html_instructions'],
            }

            google_route.append(point_route)

        if str_mode == 'rotafacil':

            points_selected = lib.map.select_points(array_line, google_route)

            if str_radius != '':
                points_with_references = lib.map.set_references_to_points(points_selected, float(str_radius))
            else:
                points_with_references = lib.map.set_references_to_points(points_selected)

            points_with_context = lib.map.set_contexts(points_with_references)

            if str_address != '' and str_number != '':
                final_points, places_list = lib.map.get_final(points_with_context, bool(int(str_address)), int(str_number))
            else:
                final_points, places_list = lib.map.get_final(points_with_context)

            rf_text = lib.nlg.get_text(str_to, str_from, array_line[0], final_points)

            #places_list = lib.nlg.get_markers()
            #places_list = lib.map.get_places_list()

        else:

            rf_text = ""
            places_list = []

        return_dict = {
            'status': "OK",
            'message': "",
            'polyline': array_line,
            'google_route': google_route,
            'start_address': start_address,
            'end_address': end_address,
            'direction_id': direction_id,
            'bounds': bounds,
            'places_list': places_list,
            'total_places': len(places_list),
            'total_points': len(points_with_context),
            'rf_text': rf_text,

        }

    else:

        return_dict = {}

        if status_code == 'NOT_FOUND':
            return_dict = {
                'status': status_code,
                'message': "NOT FOUND"
            }
        elif status_code == 'ZERO_RESULTS':
            return_dict = {
                'status': status_code,
                'message': "ZERO RESULTS"
            }
        elif status_code == 'MAX_WAYPOINTS_EXCEEDED':
            return_dict = {
                'status': status_code,
                'message': "MAX WAYPOINTS EXCEEDED"
            }
        elif status_code == 'INVALID_REQUEST':
            return_dict = {
                'status': status_code,
                'message': "INVALID REQUEST"
            }
        elif status_code == 'OVER_QUERY_LIMIT':
            return_dict = {
                'status': status_code,
                'message': "OVER QUERY LIMIT"
            }
        elif status_code == 'REQUEST_DENIED':
            return_dict = {
                'status': status_code,
                'message': "REQUEST DENIED"
            }
        elif status_code == 'UNKNOWN_ERROR':
            return_dict = {
                'status': status_code,
                'message': "UNKNOWN ERROR"
            }

    return_dict = json.dumps(return_dict)

    return StreamingHttpResponse(return_dict, content_type="application/json")


# TODO: Analisar a utilidade desta função
def get_directions(request):

    str_from = request.GET.get('from','')
    str_to = request.GET.get('to','')
    str_id = request.GET.get('id','')
    str_mode = request.GET.get('mode','')

    rf = RotaFacil()

    if(str_id != ''):
        retorno_directions = rf.getDirections('','',int(str_id))
    else:
        retorno_directions = rf.getDirections(str_from, str_to)

    retorno = json.dumps(retorno_directions)

    return StreamingHttpResponse(retorno,content_type="text/javascript")


def public(request):

    str_submit = request.GET.get('submit', '')

    PUBLIC_PAGE = PROJECT_PATH+"/projects/rota_facil/templates/public/home.html"

    args = {'projectid': PROJECT_ID, 'projectname': PROJECT_NAME, 'urlpage': PUBLIC_PAGE}

    config = {
        'map_center': {'lat': -10.947247, 'lng': -37.073082},
        'has_auto_complete': True,
        'auto_complete_elements': ['from', 'to']
    }

    rf = RotaFacil(config)

    if str_submit != '':
        str_from = request.POST.get('from', '')
        str_to = request.POST.get('to', '')
        rf.getDirections(str_from, str_to)
        args['submit'] = True
    else:
        args['submit'] = False

    google_js = rf.googleJS()
    rotafacil_js = rf.rotaFacilJS()
    draw_map = rf.drawMap()

    args['google_js'] = google_js
    args['rotafacil_js'] = rotafacil_js
    args['draw_map'] = draw_map
    args['rf_text'] = rf.rotaFacilText()

    return render(request, URL_WEB, args)

@csrf_exempt
def get_google_places(request):

    lat = request.GET.get('lat', '')
    lng = request.GET.get('lng', '')
    radius = request.GET.get('radius', '')

    if radius == '':
        radius = lib.map.RADIUS_SEARCH

    lat = float(lat)
    lng = float(lng)

    try:

        places, message = lib.map.get_places(lat, lng, radius)

        status = places['status']

        if status == "OK":

            google_places = places['results']

            place_points = []



            for place in google_places:

                point = {
                    'name': place['name'],
                    'latitude': place['geometry']['location']['lat'],
                    'longitude': place['geometry']['location']['lng'],
                    'address': place['vicinity'],
                    'types': place['types'],
                    'icon': place['icon'],
                }

                place_points.append(point)

                dict_final = {
                    'status': "OK",
                    'message': message,
                    'places': place_points
                }

        else:

            place_points = []

            dict_final = {
                'status': status,
                'message': message,
                'places': place_points
            }

    except BaseException as e:

        place_points = []

        dict_final = {
            'status': "ERROR_EXCEPTION",
            'message': e.args[0],
            'places': place_points
        }

    return_value = json.dumps(dict_final)

    return HttpResponse(return_value, content_type="text/javascript")