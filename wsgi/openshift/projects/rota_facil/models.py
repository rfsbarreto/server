from django.db import models

class LatLngCaching(models.Model):

    id = models.AutoField(primary_key=True);
    address = models.CharField(max_length=1000)
    latitude = models.FloatField()
    longitude = models.FloatField()

    def get_dict(self):
        return {'id': self.id, 'lat': self.latitude, 'lng': self.longitude, 'address': self.address}

    def get_address(self):
        return self.address

    def get_point(self):
        return {'lat': self.latitude, 'lng': self.longitude}

    def get_id(self):
        return self.id


class GeocodeCaching(models.Model):

    id = models.AutoField(primary_key=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    address = models.CharField(max_length=1000)


    def get_dict(self):
        return {'id': self.id, 'lat': self.latitude, 'lng': self.longitude, 'address': self.address}

    def get_address(self):
        return self.address

    def get_point(self):
        return {'lat': self.latitude, 'lng': self.longitude}

    def get_id(self):
        return self.id

class DirectionsCaching(models.Model):

    id = models.AutoField(primary_key=True)

    origin = models.CharField(max_length=1000)
    destination = models.CharField(max_length=1000)

    directions = models.TextField()

    def get_id(self):
        return self.id

    def get_directions(self):
        return self.directions



