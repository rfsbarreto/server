from django.conf.urls import patterns, include, url


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('projects.rota_facil.views',
    # Examples:
    url(r'^$', 'home', name='home_rotafacil'),
    url(r'public/$','public',name='public_rotafacil'),
    url(r'public/get_route/$','get_route',name='public_getroute'),
    url(r'request/$','get_route',name='request'),
    url(r'places/$','get_google_places',name='places'),
    url(r'json/$', 'get_directions', name='directions'),
    url(r'address/$', 'get_address', name='address')
)