from django.conf.urls import patterns, include, url


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('projects.go_track.views',
    # Examples:
    url(r'^$', 'home', name='home_gotrack'),
     url(r'public/$','public',name='public_gotrack'),
    url(r'record/$','record_track',name='record'),
    url(r'teste/$','teste_track',name='teste'),
    url(r'server/$','teste_server',name='teste_server'),
)