'''
@author: Adolfo Guimaraes
@description: Generate a public version of Go! Track Dataset.
'''

import csv
from datetime import datetime
from datetime import timedelta

dict_devices = {}
dict_final = []
dict_final_points = []

DATASET_VERSION = 2.0

def convert(value):
    if value == 1:
        return 1
    else:
        return 2


with open("in/" + str(DATASET_VERSION) + "/go_track_tracks.csv","rb") as csvfile:

    linereader = csv.DictReader(csvfile,delimiter=";")

    count = 0

    for r in linereader:

        dict_final.append(r)

        if not dict_devices.has_key(r['id_android']):
            dict_devices[r['id_android']] = count
            count += 1


for d in dict_final:
    d['id'] = int(d['id'])
    d['id_android'] = dict_devices[d['id_android']]
    d['speed'] = float(d['speed'])
    d['distance'] = float(d['distance'])
    d['time'] = float(d['time'])
    d['rating'] = int(d['rating'])
    d['rating_bus'] = int(d['rating_bus'])
    d['rating_weather'] = int(d['rating_weather'])
    d['car_or_bus'] = convert(int(d['car_or_bus']))
    d['linha'] = str(d['linha'])



with open('out/' + str(DATASET_VERSION) + '/go_track_tracks.csv', 'w') as csvfilew:
    header = ['id', 'id_android', 'speed', 'time', 'distance', 'rating', 'rating_bus', 'rating_weather', 'car_or_bus', 'linha']
    writer = csv.DictWriter(csvfilew, fieldnames=header, delimiter=";",quoting=csv.QUOTE_NONNUMERIC)

    writer.writeheader()

    writer.writerows(dict_final)


with open("in/" + str(DATASET_VERSION) + "/go_track_trackspoints.csv","rb") as csvfile:

    linereader = csv.DictReader(csvfile,delimiter=";")


    count = 0

    for r in linereader:

        dict_final_points.append(r)


for d in dict_final_points:

    d['id'] = int(d['id'])
    d['longitude'] = float(d['longitude'])
    d['latitude'] = float(d['latitude'])
    d['track_id'] = int(d['track_id'])
    d['time'] = datetime.strptime(d['time'], '%Y-%m-%d %H:%M:%S') + timedelta(hours=-3)

with open('out/' + str(DATASET_VERSION) + '/go_track_trackspoints.csv', 'w') as csvfilew:
    header = ['id','latitude','longitude','track_id','time']
    writer = csv.DictWriter(csvfilew, fieldnames=header, delimiter=";",quoting=csv.QUOTE_NONNUMERIC)

    writer.writeheader()

    writer.writerows(dict_final_points)
