Basic Information: 

Total of Trajectories: 163
Total of Points: 18107
Total of Devices: 28


This dataset contains two files:

(1) go_track_tracks.csv: a list of routes

Fields:

id: unique key to identify each route.
id_android: an identifier for each device that was used to collect routes.
speed: average speed during all pathway.
time: the durantion of the pathway in minutes.
distance: distance of the route in kilometer.
rating: the user evaluation about the traffic.
rating_bus: indicates the quality of the travel (available just in bus case)
rating_wheather: indicates the conditions of the weather (available just in bus case)
car_or_bus: indicates if the route was collected by a car or a bus
linha: information about the bus that does the pathway (available just in bus case). 

(2) go_track_trackspoints.csv: localization points of each trajectory

Fields:

id: unique key to identify each point
latitude: latitude from where the point is
longitude: longitude from where the point is
track_id: identify the route which the point belong
time: datetime when the point was collected (GMT-3)