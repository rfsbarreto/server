from rest_framework import serializers
from projects.go_track.models import Tracks, TracksPoints


class TracksPointsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TracksPoints
        fields = ('id','latitude','longitude','time')

class TracksSerializer(serializers.ModelSerializer):
    tracks_points = TracksPointsSerializer(many=True)

    class Meta:
        model = Tracks
        fields = ('id','id_android','time','distance','speed','rating','linha','car_or_bus','rating_weather','rating_bus','tracks_points')