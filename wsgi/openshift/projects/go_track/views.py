from django.shortcuts import render
from django.conf import settings
import os.path
import time

import json
import requests
import register.lib.projects as r_projects
import operator


from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from models import Tracks, TracksPoints
from lib.serializers import TracksSerializer

from register.models import Projects, UserProject

from django.db.models import Count

from projects.rota_facil.lib.RotaFacil import RotaFacil
import projects.rota_facil.lib.map as map

class JSONResponse(HttpResponse):

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    PROJECT_PATH = os.environ['OPENSHIFT_REPO_DIR']+"wsgi/openshift"
else:
    PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

URL_PAGE = PROJECT_PATH+"/projects/go_track/templates/project_page.html"
URL_WEB = PROJECT_PATH+"/templates/project.html"
PROJECT_NAME = "GO! Track"
PROJECT_ID = "go_track"

def get_type(type):
    if type == 0: return u"onibus"
    elif type == 1: return u"carro"
    elif type == 2: return u"onibus"

def home(request):
    if not request.user.is_authenticated():
        return render(request, "index.html",{'status':'noactive'})
    else:
        args = {'projectid':PROJECT_ID, 'projectname':PROJECT_NAME, 'urlpage': URL_PAGE}
        args['packages_dir'] = settings.PACKAGES_DIR

        args['list_user_projects'] = r_projects.get_users_of_project(PROJECT_ID)


        tracks = Tracks.objects.all()
        total_routes = len(tracks)

        alltracks = []

        for t in tracks:
            dictrack = {
                'id': t.id,
                'tipo': get_type(t.car_or_bus)
            }

            alltracks.append(dictrack)


        devices = Tracks.objects.values('id_android').annotate(dcount=Count('id_android'))

        tracks_points = TracksPoints.objects.count()

        args['devices_total'] = len(devices)
        args['routes_total'] = total_routes
        args['points_average'] = float(tracks_points) / float(total_routes)


        config = {
            'map_center': {'lat': -10.947247, 'lng': -37.073082},
            'has_auto_complete': True,
            'map_width': 680,
            'map_height': 600,
            'auto_complete_elements': ['from','to']
        }

        rf = RotaFacil(config)

        str_submit = request.GET.get('submit','')
        str_filter = request.POST.get('filter')

        if(str_submit == "track"):
            id_track = request.POST.get('select_tracks')
            track = Tracks.objects.get(id=id_track)
            points = TracksPoints.objects.filter(track_id=id_track)
            polyline = []
            for p in points:
                point = [p.latitude, p.longitude]
                polyline.append(point)

            args['track'] = track
            args['track_time'] = "{0:.2f}".format(track.time * 60)
            args['track_distance'] = "{0:.2f}".format(track.distance)
            args['track_speed'] = "{0:.2f}".format(track.speed)
            args['date_start'] = points[0].time
            args['date_finished'] = points[len(points) - 1].time

            dict_address = {}

            for p in polyline:

                address = rf.getAddress(p)

                if dict_address.has_key(address):
                    dict_address[address] += 1
                else:
                    dict_address[address] = 1

            #print dict_address

            rf.showPolyline(polyline)

        elif str_submit == "all_points":
            q_car = TracksPoints.objects.filter(track__car_or_bus=1)
            q_bus = TracksPoints.objects.filter(track__car_or_bus=2)
            q_bus2 = TracksPoints.objects.filter(track__car_or_bus=0)

            points_list = []

            args['all_points'] = True

            addressreport = request.POST.get('addressreport')


            if str_filter == "car" or str_filter == "both":

                args['total_cars'] = len(q_car)

                for p in q_car:

                    dict_p = {
                        'track_id': p.track_id,
                        'longitude': p.longitude,
                        'latitude': p.latitude,
                        'tipo': 1,
                        'time': p.time
                    }

                    points_list.append(dict_p)

            if str_filter == "bus" or str_filter == "both":

                args['total_bus'] = len(q_bus)

                for p in q_bus:

                    dict_p = {
                        'track_id': p.track_id,
                        'longitude': p.longitude,
                        'latitude': p.latitude,
                        'tipo': 2,
                        'time': p.time
                    }

                    points_list.append(dict_p)

            if str_filter == "bus" or str_filter == "both":

                args['total_bus'] += len(q_bus2)

                for p in q_bus2:

                    dict_p = {
                        'track_id': p.track_id,
                        'longitude': p.longitude,
                        'latitude': p.latitude,
                        'tipo': 0,
                        'time': p.time
                    }

                    points_list.append(dict_p)


            rf.markAllPoints(points_list)

            if addressreport == "address":

                dict_address = {}
                dict_address_track = {}
                dict_address_pointlist = {}
                dict_address_pointlist_id = {}

                count = len(points_list)

                for p in points_list:

                    address = map.latlng_to_address(p['latitude'], p['longitude'])

                    #print count, p, address

                    count -= 1

                    if dict_address_pointlist.has_key(address):
                        if p not in dict_address_pointlist[address]:
                            dict_address_pointlist[address].append(p['time'])
                            dict_address_pointlist_id[address].append({'time': p['time'], 'track_id': p['track_id']})
                    else:
                        dict_address_pointlist[address] = [p['time']]
                        dict_address_pointlist_id[address] = [{'time': p['time'], 'track_id': p['track_id']}]


                    if dict_address.has_key(address):
                        dict_address[address] += 1
                    else:
                        dict_address[address] = 1


                    if dict_address_track.has_key(address):
                        if p['track_id'] not in dict_address_track[address]:
                            dict_address_track[address].append(p['track_id'])
                    else:
                        dict_address_track[address] = [p['track_id']]


                sorted_dict = dict_address.items() #sorted(dict_address.items(), key=operator.itemgetter(1), reverse=True)

                final_list = []

                for s in sorted_dict:
                    final_list.append((s[0], s[1], len(dict_address_track[s[0]]), dict_address_pointlist[s[0]],
                                       dict_address_pointlist_id[s[0]]))

                final_list = sorted(final_list, key=operator.itemgetter(2), reverse=True)

                args['number_of_address'] = len(final_list)
                args['sorted_address'] = final_list

                json.dump(dict_address_pointlist, open("teste2.txt", 'w'))
                json.dump(final_list, open("teste3.txt", 'w'))


        google_js = rf.googleJS()
        rotafacil_js = rf.rotaFacilJS()
        draw_map = rf.drawMap()


        args['tracks'] = alltracks
        args['google_js'] = google_js
        args['rotafacil_js'] = rotafacil_js
        args['draw_map'] = draw_map



        return render(request, URL_WEB, args)





@csrf_exempt
def record_track(request):

    if request.method == 'GET':
        tracks = Tracks.objects.all()
        serializer = TracksSerializer(tracks, many=True)
        return JSONResponse(serializer.data)

    if request.method == 'POST':

        data = JSONParser().parse(request)

        serializer = TracksSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)

@csrf_exempt
def teste_server(request):

    if request.method == 'GET':
        print "Receive information by GET"

        print request

    if request.method == 'POST':
        print "Receive information by POST"

        print request

    args = {'projectid':PROJECT_ID, 'projectname':PROJECT_NAME, 'urlpage': URL_PAGE}
    args['packages_dir'] = settings.PACKAGES_DIR
    return render(request, URL_WEB, args)


def teste_track(request):

    track_point = []


    point1 = {
        'latitude': 1.0,
        'longitude': 2.0,
        'time': time.strftime('%Y-%m-%d %H:%M:%S')
    }

    point2 = {
        'latitude': 2.0,
        'longitude': 1.0,
        'time': time.strftime('%Y-%m-%d %H:%M:%S')
    }


    track_point.append(point1)
    track_point.append(point2)

    track = {
        'time': time.strftime('%Y-%m-%d %H:%M:%S'),
        'distance': 2.0,
        'speed': 10.0,
        'rating': 10,
        'linha':'Desembargador',
        'car_or_bus': 5,
        'rating_weather':1,
        'rating_bus':1,
        'tracks_points': track_point,
    }

    data_json = json.dumps(track)
    headers = {'Content-type': 'application/json'}
    response = requests.post('http://go-goproject.rhcloud.com/go/go_track/record/', data=data_json, headers=headers)

    args = {'projectid':PROJECT_ID, 'projectname':PROJECT_NAME, 'urlpage': URL_PAGE}
    args['packages_dir'] = settings.PACKAGES_DIR
    return render(request, URL_WEB, args)


def public(request):

    str_submit = request.GET.get('submit','')

    PUBLIC_PAGE = PROJECT_PATH+"/projects/go_track/templates/public/home.html"

    args = {'projectid':PROJECT_ID, 'projectname':PROJECT_NAME, 'urlpage': PUBLIC_PAGE}




    return render(request, URL_WEB, args)

