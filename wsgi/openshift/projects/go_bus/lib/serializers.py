from rest_framework import serializers
from projects.go_bus.models import TracksLastPoints


class TracksPointsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TracksLastPoints
        fields = ('latitude','longitude','id_line','speed','time','id_mobile')


