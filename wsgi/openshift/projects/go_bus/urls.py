from django.conf.urls import patterns, include, url


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('projects.go_bus.views',
    # Examples:
    url(r'^$', 'home', name='home_gobus'),
    url(r'public/$','public',name='public_bus'),
    url(r'record/$','points',name='get_last_points'),
)
